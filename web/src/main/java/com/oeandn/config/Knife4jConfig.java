package com.oeandn.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @Author：donnie
 * @Description: 接口文档配置
 */
@EnableSwagger2WebMvc
@Configuration
public class Knife4jConfig {


    @Bean
    public Docket defaultApi1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("文件管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oeandn.file.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket defaultApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("系统管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oeandn.modules.system.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket defaultApi3() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("巡检管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oeandn.modules.inspect.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("智能巡检项目")
                .contact(new Contact("donnie", "https://gitee.com/donnie421", "@qq.com"))
                .version("1.0")
                .build();
    }
}
