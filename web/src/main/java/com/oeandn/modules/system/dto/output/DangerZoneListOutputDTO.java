package com.oeandn.modules.system.dto.output;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 危险区-红橙区
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="DangerZoneListOutputDTO", description="危险区-红橙区列表")
public class DangerZoneListOutputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "红橙区名称")
    private String zoneName;

    @ApiModelProperty(value = "房间号")
    private String roomNo;

    @ApiModelProperty(value = "坐标1")
    private String coordOne;

    @ApiModelProperty(value = "坐标2")
    private String coordTwo;

    @ApiModelProperty(value = "坐标3")
    private String coordThree;

    @ApiModelProperty(value = "坐标4")
    private String coordFour;

    @ApiModelProperty(value = "1橙区2红区")
    private String zoneTag;

    @ApiModelProperty(value = "创建者")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新者")
    private Long updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "部门id")
    private Long deptId;

}
