package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.DeptCreateInputDTO;
import com.oeandn.modules.system.dto.input.DeptSearchInputDTO;
import com.oeandn.modules.system.dto.input.DeptUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DeptListOutputDTO;
import com.oeandn.system.model.Dept;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper(componentModel = "spring")
public interface MDeptMapper {

    Dept toDept(DeptSearchInputDTO deptSearchInputDTO);

    DeptListOutputDTO toOutput(Dept dept);

    List<DeptListOutputDTO> toOutputList(List<Dept> depts);

    /**
     * 添加表单转换
     *
     * @param deptCreateInputDTO
     * @return
     */
    Dept toDept(DeptCreateInputDTO deptCreateInputDTO);

    /**
     * 修改表单转换
     *
     * @param deptUpdateInputDTO
     * @return
     */
    Dept toDept(DeptUpdateInputDTO deptUpdateInputDTO);
}
