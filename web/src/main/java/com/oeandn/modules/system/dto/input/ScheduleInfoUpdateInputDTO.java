package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 排班
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="ScheduleInfoUpdateInputDTO", description="排班修改表单")
public class ScheduleInfoUpdateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "早班，1值,2值,3值，多个逗号隔开")
    private String morningShift;

    @ApiModelProperty(value = "中班，1值,2值,3值，多个逗号隔开")
    private String noonShift;

    @ApiModelProperty(value = "晚班，1值,2值,3值，多个逗号隔开")
    private String eveningShift;

}
