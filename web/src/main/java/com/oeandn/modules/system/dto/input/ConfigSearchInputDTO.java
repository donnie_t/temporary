package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 15:07
 */
@Getter
@Setter
@ApiModel(value = "ConfigSearchInputDTO", description = "配置查询使用")
public class ConfigSearchInputDTO {

    @ApiModelProperty(value = "参数名称")
    private String configName;
}
