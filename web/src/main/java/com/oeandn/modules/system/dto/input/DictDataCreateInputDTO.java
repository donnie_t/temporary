package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 字典数据表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Getter
@Setter
@ApiModel(value="DictDataCreateInputDTO", description="字典数据添加表单")
public class DictDataCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级id")
    @NotBlank
    private Long parentId;

    @ApiModelProperty(value = "字典标签")
    @NotBlank
    private String dictLabel;

    @ApiModelProperty(value = "字典键值")
    @NotBlank
    private String dictValue;



    @ApiModelProperty(value = "字典排序")
    @NotBlank
    private Integer dictSort;

    @ApiModelProperty(value = "状态（0正常 1停用）-字典")
    @NotBlank
    private String dictStatus;


}
