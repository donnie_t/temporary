package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.DictTypeCreateInputDTO;
import com.oeandn.modules.system.dto.input.DictTypeSearchInputDTO;
import com.oeandn.modules.system.dto.input.DictTypeUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DictTypeListOutputDTO;
import com.oeandn.system.model.DictType;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper(componentModel = "spring")
public interface MDictTypeMapper {

    DictType dictTypeSearchInputDTOToDictType(DictTypeSearchInputDTO dictTypeSearchInputDTO);

    DictType dictTypeCreateInputDTOToDictType(DictTypeCreateInputDTO dictTypeCreateInputDTO);

    DictType dictTypeUpdateInputDTOToDictType(DictTypeUpdateInputDTO dictTypeUpdateInputDTO);

    DictTypeListOutputDTO dictTypeToDictTypeListOutputDTO(DictType dictType);

    List<DictTypeListOutputDTO> dictTypesToDictTypeListOutputDTOs(List<DictType> dictTypes);

}
