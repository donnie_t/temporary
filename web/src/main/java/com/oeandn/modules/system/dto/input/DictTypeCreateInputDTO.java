package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 字典类型表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Getter
@Setter
@ApiModel(value="DictTypeCreateInputDTO", description="字典类型添加使用")
public class DictTypeCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典名称")
    @NotBlank
    private String dictName;



    @ApiModelProperty(value = "状态（0正常 1停用）-字典")
    @NotBlank
    private String dictStatus;



}
