package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.ScheduleInfoCreateInputDTO;
import com.oeandn.modules.system.dto.input.ScheduleInfoSearchInputDTO;
import com.oeandn.modules.system.dto.input.ScheduleInfoUpdateInputDTO;
import com.oeandn.modules.system.dto.output.ScheduleInfoListOutputDTO;
import com.oeandn.modules.system.dtomapper.MScheduleInfoMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.ScheduleInfo;
import com.oeandn.system.service.ScheduleInfoService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 排班 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/schedule-info")
@Api(value = "ScheduleInfoController", tags = {"排班管理"})
public class ScheduleInfoController extends BaseController<ScheduleInfoService,ScheduleInfo> {

    private final MScheduleInfoMapper mScheduleInfoMapper;

    @Log
    @ApiOperation(value = "排班查询")
    @GetMapping("/list")
    public R<List<ScheduleInfoListOutputDTO>> list(ScheduleInfoSearchInputDTO scheduleInfoSearchInputDTO) {
        // 转换model
        ScheduleInfo scheduleInfo = mScheduleInfoMapper.toScheduleInfo(scheduleInfoSearchInputDTO);
        // 获取源对象
        List<ScheduleInfo> pageListByListInputDTO = baseService.list(scheduleInfo);
        // 转换新对象
        List<ScheduleInfoListOutputDTO> scheduleInfoListOutputDTOS = mScheduleInfoMapper.toOutputList(pageListByListInputDTO);
        // 返回业务分页数据
        return success(scheduleInfoListOutputDTOS);
    }


    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ScheduleInfoUpdateInputDTO scheduleInfoUpdateInputDTO) {
        ScheduleInfo scheduleInfo = mScheduleInfoMapper.toScheduleInfo(scheduleInfoUpdateInputDTO);
        return super.update(scheduleInfo);
    }


}
