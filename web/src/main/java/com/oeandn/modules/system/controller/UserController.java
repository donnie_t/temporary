package com.oeandn.modules.system.controller;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.common.model.R;
import com.oeandn.common.utils.BPwdEncoderUtil;
import com.oeandn.common.utils.EasyExcelUtil;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.db.base.util.ViewMapper;
import com.oeandn.enums.AdminApiErrorCode;
import com.oeandn.modules.system.dto.input.*;
import com.oeandn.modules.system.dto.output.*;
import com.oeandn.modules.system.dtomapper.MPostMapper;
import com.oeandn.modules.system.dtomapper.MRoleMapper;
import com.oeandn.modules.system.dtomapper.MUserInfoMapper;
import com.oeandn.modules.system.service.AuthService;
import com.oeandn.system.model.Dept;
import com.oeandn.system.model.Post;
import com.oeandn.system.model.Role;
import com.oeandn.system.model.UserInfo;
import com.oeandn.system.service.DeptService;
import com.oeandn.system.service.PostService;
import com.oeandn.system.service.RoleService;
import com.oeandn.system.service.UserInfoService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/8/26 9:09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/user")
@ApiSupport(order = 2)
@Api(value = "UserController", tags = {"用户管理"})
public class UserController extends BaseController<UserInfoService, UserInfo> {

    private static final BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private final MUserInfoMapper mUserInfoMapper;
    private final MPostMapper mPostMapper;
    private final MRoleMapper mRoleMapper;
    private final AuthService authService;
    private final PostService postService;
    private final DeptService deptService;

    @Log
    @ApiOperation(value = "用户分页查询")
    @ApiOperationSupport(order = 1, author = "饮水机管理员")
    @GetMapping("/page")
    public R<PageResult<UserInfoListOutputDTO>> page(UserInfoSearchInputDTO userInfoSearchInputDTO, PageModel pageModel) {
        // 转换model
        UserInfo userInfo = mUserInfoMapper.userInfoSearchInputDTOToUserInfo(userInfoSearchInputDTO);
        // 获取源对象
        IPage<UserInfo> pageListByListInputDTO = baseService.pageList(userInfo, pageModel);
        // 转换新对象
        List<UserInfoListOutputDTO> userInfoListOutputDTOS = mUserInfoMapper.userInfosToUserInfoListOutputDTOs(pageListByListInputDTO.getRecords());

        ViewMapper.batchMapView(userInfoListOutputDTOS,
                ViewMapper.setView(
                        this::getMapDept,
                        UserInfoListOutputDTO::getDeptId,
                        ((userInfoListOutputDTO, dept) -> userInfoListOutputDTO.setDeptName(dept.getName()))));
        // 返回业务分页数据
        userInfoListOutputDTOS.forEach(i->{
            List<Role> roleListByUserId = baseService.getAuthRoleListByUserId(i.getId());
            if(roleListByUserId!=null && !roleListByUserId.isEmpty()){
                List<RoleListOutputDTO> roleListOutputDTOS = mRoleMapper.rolesToRoleListOutputDTOs(roleListByUserId);
                String collect = roleListByUserId.stream().map(Role::getRoleName).collect(Collectors.joining(","));
                i.setRoleName(collect);
                i.setRoleList(roleListOutputDTOS);
            }

            List<Long> postListByUserId = baseService.getPostListByUserId(i.getId());
            if(postListByUserId!=null && !postListByUserId.isEmpty()){
                List<Post> list = postService.list(Wrappers.<Post>lambdaQuery().in(Post::getId,postListByUserId));
                List<PostListOutputDTO> postListOutputDTOS = mPostMapper.postsToPostListOutputDTOs(list);
                String postName = list.stream().map(Post::getPostName).collect(Collectors.joining(","));
                i.setPostName(postName);
                i.setPostList(postListOutputDTOS);
            }

        });
        return success(toPageDTO(pageListByListInputDTO, userInfoListOutputDTOS));
    }


    private Map<Long, Dept> getMapDept(Collection<Long> ids) {
        return deptService.listByIds(ids)
                .stream().collect(Collectors.toMap(Dept::getId, dept2 -> dept2));
    }

    @Log
    @ApiOperation(value = "添加")
    @ApiOperationSupport(order = 5, author = "饮水机管理员")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody UserInfoCreateInputDTO userInfoCreateInputDTO) {
        UserInfo userInfo = mUserInfoMapper.userInfoCreateInputDTOToUserInfo(userInfoCreateInputDTO);
        userInfo.setPassword(PASSWORD_ENCODER.encode(userInfoCreateInputDTO.getPassword()));
        boolean bool = baseService.saveUser(userInfo, userInfoCreateInputDTO.getPostIds(),userInfoCreateInputDTO.getRoleIds());
        if (bool) {
            authService.refreshUserInfoByUserId(userInfo.getId());
        }
        return R.success(bool);
    }

    @Log
    @ApiOperation(value = "修改")
    @ApiOperationSupport(order = 10, author = "饮水机管理员")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody UserInfoUpdateInputDTO userInfoUpdateInputDTO) {
        UserInfo userInfo = mUserInfoMapper.userInfoUpdateInputDTOToUserInfo(userInfoUpdateInputDTO);
        userInfo.setPassword(PASSWORD_ENCODER.encode(userInfoUpdateInputDTO.getPassword()));
        boolean bool = baseService.saveUser(userInfo, userInfoUpdateInputDTO.getPostIds(),userInfoUpdateInputDTO.getRoleIds());
        if (bool) {
            authService.refreshUserInfoByUserId(userInfo.getId());
        }
        return R.success(bool);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiOperationSupport(order = 15, author = "饮水机管理员")
    @ApiImplicitParam(name = "id", value = "用户id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "用户id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @ApiOperationSupport(order = 20, author = "饮水机管理员")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

    @Log
    @ApiOperation(value = "重置密码")
    @ApiOperationSupport(order = 25, author = "饮水机管理员")
    @PostMapping(value = "/resetPassword")
    public R<Boolean> resetPassword(@Validated @ApiParam @RequestBody UserInfoResetPassInputDTO userInfoResetPassInputDTO) {
        UserInfo userInfo = mUserInfoMapper.userInfoResetPassInputDTOToUserInfo(userInfoResetPassInputDTO);
        userInfo.setPassword(PASSWORD_ENCODER.encode(userInfoResetPassInputDTO.getPassword()));
        return super.update(userInfo);
    }

    @Log
    @ApiOperation(value = "根据用户名获取用户信息")
    @ApiOperationSupport(order = 30, author = "饮水机管理员")
    @ApiImplicitParam(name = "username", value = "用户名", dataType = "Long", paramType = "query", required = true)
    @GetMapping(value = "/getUserInfoByUsername")
    public R<UserInfoListOutputDTO> getUserInfoByUsername(@NotBlank(message = "用户名不能为空") @RequestParam String username) {
        UserInfo userInfo = baseService.getUserInfoByUsername(username);
        UserInfoListOutputDTO userInfoListOutputDTO = mUserInfoMapper.userInfoToUserInfoListOutputDTO(userInfo);
        return success(userInfoListOutputDTO);
    }


    @Log
    @ApiOperation(value = "修改基本信息")
    @ApiOperationSupport(order = 50, author = "饮水机管理员")
    @PostMapping(value = "/updateInfo")
    public R<Boolean> updateInfo(@Validated @ApiParam @RequestBody UserInfoUpdateInfoInputDTO userInfoUpdateInfoInputDTO) {
        UserInfo userInfo = mUserInfoMapper.userInfoUpdateInfoInputDTOToUserInfo(userInfoUpdateInfoInputDTO);
        boolean bool = baseService.updateById(userInfo);
        return R.success(bool);
    }

    @Log
    @PostMapping("/updatePass")
    @ApiOperation(value = "修改密码")
    @ApiOperationSupport(order = 55, author = "饮水机管理员")
    public R<Boolean> updatePass(@Validated @ApiParam @RequestBody UserUpdatePassInputDTO userUpdatePassInputDTO) {
        UserInfo userInfo = baseService.getById(userUpdatePassInputDTO.getUserId());
        if (!BPwdEncoderUtil.matches(userUpdatePassInputDTO.getCurrentPass(), userInfo.getPassword())) {
            return R.error(AdminApiErrorCode.USER_UPDATE_PASS_ERROR);
        }
        if (!userUpdatePassInputDTO.getNewPass().equalsIgnoreCase(userUpdatePassInputDTO.getConfPass())) {
            return R.error(AdminApiErrorCode.USER_UPDATE_PASS2_ERROR);
        }
        UserInfo updatePassUser = new UserInfo();
        updatePassUser.setId(userUpdatePassInputDTO.getUserId());
        updatePassUser.setPassword(PASSWORD_ENCODER.encode(userUpdatePassInputDTO.getNewPass()));
        return R.success(baseService.updateById(updatePassUser));
    }

    @GetMapping("/getOnLineUserList")
    @ApiOperation(value = "获取在线用户列表")
    @ApiOperationSupport(order = 60, author = "饮水机管理员")
    public R<List<UserInfo>> getOnLineUserList() {
        List<String> list = StpUtil.searchSessionId("", 0, 100);
        final int index = 3;
        List<String> collect = list.stream().map(s -> {
            List<String> split = StrUtil.split(s, ':');
            if (StrUtil.isNotBlank(CollUtil.get(split, index))) {
                return CollUtil.get(split, index);
            }
            return null;
        }).collect(Collectors.toList());
        return R.success(baseService.listByIds(collect));
    }


}
