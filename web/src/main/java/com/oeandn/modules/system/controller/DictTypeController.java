package com.oeandn.modules.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.DictTypeCreateInputDTO;
import com.oeandn.modules.system.dto.input.DictTypeSearchInputDTO;
import com.oeandn.modules.system.dto.input.DictTypeUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DictTypeListOutputDTO;
import com.oeandn.modules.system.dtomapper.MDictTypeMapper;
import com.oeandn.system.model.DictData;
import com.oeandn.system.model.DictType;
import com.oeandn.system.service.DictDataService;
import com.oeandn.system.service.DictTypeService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/dict-type")
@ApiSupport(order = 20)
@Api(value = "DictTypeController", tags = {"字典管理"})
public class DictTypeController extends BaseController<DictTypeService, DictType> {

    private final MDictTypeMapper mDictTypeMapper;
    private final DictDataService dictDataService;

    @Log
    @ApiOperation(value = "字典类型分页查询")
    @GetMapping("/page")
    public R<PageResult<DictTypeListOutputDTO>> pageList(DictTypeSearchInputDTO dictTypeSearchInputDTO, PageModel pageModel) {
        // 转换model
        DictType dictType = mDictTypeMapper.dictTypeSearchInputDTOToDictType(dictTypeSearchInputDTO);
        // 获取源对象
        IPage<DictType> pageListByListInputDTO = baseService.pageListByEntity(dictType,pageModel);
        // 转换新对象
        List<DictTypeListOutputDTO> userInfoListOutputDTOS = mDictTypeMapper.dictTypesToDictTypeListOutputDTOs(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, userInfoListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DictTypeCreateInputDTO dictTypeCreateInputDTO) {
        DictType dictType = mDictTypeMapper.dictTypeCreateInputDTOToDictType(dictTypeCreateInputDTO);
        return R.success(baseService.save(dictType));
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DictTypeUpdateInputDTO dictTypeUpdateInputDTO) {
        DictType dictType = mDictTypeMapper.dictTypeUpdateInputDTOToDictType(dictTypeUpdateInputDTO);
        return R.success(baseService.updateById(dictType));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "字典id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "字典id不能为空") @RequestParam Long id) {
        return success(baseService.deleteDictTypeById(id));
    }

}
