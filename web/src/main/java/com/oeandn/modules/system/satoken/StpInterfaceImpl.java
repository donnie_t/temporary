package com.oeandn.modules.system.satoken;

import cn.dev33.satoken.stp.StpInterface;
import com.oeandn.modules.system.service.AuthService;
import com.oeandn.system.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：饮水机管理员
 * @Description: 自定义权限验证接口扩展
 * @Date: 2021/4/7 8:15
 */
@Component
public class StpInterfaceImpl implements StpInterface {

    @Autowired
    private AuthService authService;

    /**
     * 返回账号拥有的权限码
     *
     * @param loginId
     * @param loginKey
     * @return
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginKey) {
        return authService.getPermissionListByUserId(Long.parseLong(loginId.toString()));
    }

    /**
     * 返回账号拥有的角色标识
     *
     * @param loginId
     * @param loginKey
     * @return
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginKey) {
        return authService.getAuthRoleListByUserId(Long.parseLong(loginId.toString()))
                .stream()
                .map(Role::getRoleKey)
                .collect(Collectors.toList());
    }


}
