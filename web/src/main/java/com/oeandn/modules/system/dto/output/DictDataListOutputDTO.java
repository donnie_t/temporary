package com.oeandn.modules.system.dto.output;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 字典数据表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Getter
@Setter
@ApiModel(value="DictDataListOutputDTO对象", description="字典数据表格")
public class DictDataListOutputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典id")
    private Long id;

    @ApiModelProperty(value = "父级id")
    private Long parentId;

    @ApiModelProperty(value = "字典标签")
    private String dictLabel;

    @ApiModelProperty(value = "字典键值")
    private String dictValue;


    @ApiModelProperty(value = "字典排序")
    private BigDecimal dictSort;


    @ApiModelProperty(value = "状态（0正常 1停用）-字典")
    private String dictStatus;


}
