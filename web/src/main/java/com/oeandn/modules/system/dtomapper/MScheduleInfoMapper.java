package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.ScheduleInfoCreateInputDTO;
import com.oeandn.modules.system.dto.input.ScheduleInfoSearchInputDTO;
import com.oeandn.modules.system.dto.input.ScheduleInfoUpdateInputDTO;
import com.oeandn.modules.system.dto.output.ScheduleInfoListOutputDTO;
import com.oeandn.system.model.ScheduleInfo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 排班 转换类
 * @Date: 2022-08-10
 */
@Mapper(componentModel = "spring")
public interface MScheduleInfoMapper {

    ScheduleInfo toScheduleInfo(ScheduleInfoSearchInputDTO scheduleInfoSearchInputDTO);

    ScheduleInfo toScheduleInfo(ScheduleInfoCreateInputDTO scheduleInfoCreateInputDTO);

    ScheduleInfo toScheduleInfo(ScheduleInfoUpdateInputDTO scheduleInfoUpdateInputDTO);

    ScheduleInfoListOutputDTO toOutput(ScheduleInfo scheduleInfo);

    List<ScheduleInfoListOutputDTO> toOutputList(List<ScheduleInfo> scheduleInfos);
}