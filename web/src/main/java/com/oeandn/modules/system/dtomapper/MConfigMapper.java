package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.ConfigCreateInputDTO;
import com.oeandn.modules.system.dto.input.ConfigSearchInputDTO;
import com.oeandn.modules.system.dto.input.ConfigUpdateInputDTO;
import com.oeandn.modules.system.dto.output.ConfigListOutputDTO;
import com.oeandn.system.model.Config;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper(componentModel = "spring")
public interface MConfigMapper {

    Config toConfig(ConfigCreateInputDTO configCreateInputDTO);

    Config toConfig(ConfigUpdateInputDTO configUpdateInputDTO);

    Config toConfig(ConfigSearchInputDTO configSearchInputDTO);

    ConfigListOutputDTO toOutput(Config config);

    List<ConfigListOutputDTO> toOutputList(List<Config> configs);
}
