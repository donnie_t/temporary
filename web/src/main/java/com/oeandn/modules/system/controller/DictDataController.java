package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.utils.TreeUtil;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.DictDataCreateInputDTO;
import com.oeandn.modules.system.dto.input.DictDataSearchInputDTO;
import com.oeandn.modules.system.dto.input.DictDataUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DictDataListOutputDTO;
import com.oeandn.modules.system.dto.output.DictDataSelOutputDTO;
import com.oeandn.modules.system.dto.output.DictDataTreeSelOutputDTO;
import com.oeandn.modules.system.dtomapper.MDictDataMapper;
import com.oeandn.system.model.DictData;
import com.oeandn.system.model.DictType;
import com.oeandn.system.service.DictDataService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.oeandn.system.service.DictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 * 字典数据表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/dict-data")
@ApiSupport(order = 25)
@Api(value = "DictDataController", tags = {"字典管理"})
public class DictDataController extends BaseController<DictDataService, DictData> {

    private final MDictDataMapper mDictDataMapper;

    private final DictTypeService dictTypeService;


    @Log
    @ApiOperation(value = "添加字典项")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DictDataCreateInputDTO dictDataCreateInputDTO) {
        DictData dictData = mDictDataMapper.dictDataCreateInputDTOToDictData(dictDataCreateInputDTO);
        DictType byId = dictTypeService.getById(dictData.getParentId());
        return R.success(baseService.save(dictData));
    }

    @Log
    @ApiOperation(value = "修改字典项")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DictDataUpdateInputDTO dictDataUpdateInputDTO) {
        DictData dictData = mDictDataMapper.dictDataUpdateInputDTOToDictData(dictDataUpdateInputDTO);
        return R.success(baseService.updateById(dictData));
    }

    @Log
    @ApiOperation(value = "删除字典项")
    @ApiImplicitParam(name = "id", value = "字典数据id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "字典数据id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @ApiOperation(value = "根据id查询字典项")
    @ApiImplicitParam(name = "parentId", value = "字典类型", dataType = "Long", paramType = "query", required = true)
    @GetMapping(value = "/getDictDataListByDictType")
    public R<List<DictDataSelOutputDTO>> getDictDataListByDictType(@NotBlank(message = "字典id不能为空") @RequestParam Long parentId) {
        List<DictData> dictList = baseService.getDictDataListByDictType(parentId);
        List<DictDataSelOutputDTO> dictListOutputDTOS = mDictDataMapper.dictDatasToDictDataSelOutputDTOs(dictList);
        return success(dictListOutputDTOS);
    }

    @ApiOperation(value = "根据类型id查询字典项（tree数据）")
    @ApiImplicitParam(name = "parentId", value = "字典类型", dataType = "Long", paramType = "query", required = true)
    @GetMapping(value = "/getDictDataTreeListByDictType")
    public R<List<DictDataTreeSelOutputDTO>> getDictDataTreeListByDictType(@NotBlank(message = "字典类型不能为空") @RequestParam Long parentId) {
        List<DictData> dictList = baseService.getDictDataListByDictType(parentId);
        List<DictData> dictData = TreeUtil.listToTree(dictList);
        List<DictDataTreeSelOutputDTO> dictListOutputDTOS = mDictDataMapper.dictDatasToDictDataTreeSelOutputDTOs(dictData);
        return success(dictListOutputDTOS);
    }
}
