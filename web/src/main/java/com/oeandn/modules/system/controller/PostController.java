package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.PostCreateInputDTO;
import com.oeandn.modules.system.dto.input.PostSearchInputDTO;
import com.oeandn.modules.system.dto.input.PostUpdateInputDTO;
import com.oeandn.modules.system.dto.output.PostListOutputDTO;
import com.oeandn.modules.system.dtomapper.MPostMapper;
import com.oeandn.system.model.Post;
import com.oeandn.system.service.PostService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-16
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/post")
@ApiSupport(order = 30)
@Api(value = "PostController", tags = {"岗位管理"})
public class PostController extends BaseController<PostService, Post> {

    private final MPostMapper mPostMapper;

    @Log
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public R<PageResult<PostListOutputDTO>> pageList(PostSearchInputDTO postSearchInputDTO, PageModel pageModel) {
        // 转换model
        Post post = mPostMapper.postSearchInputDTOToPost(postSearchInputDTO);
        // 获取源对象
        IPage<Post> pageListByListInputDTO = baseService.pageListByEntity(post,pageModel);
        // 转换新对象
        List<PostListOutputDTO> userInfoListOutputDTOS = mPostMapper.postsToPostListOutputDTOs(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, userInfoListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "获取所有数据")
    @GetMapping("/all")
    public R<List<PostListOutputDTO>> listAll() {
        List<Post> postList = baseService.list();
        // 转换新对象
        List<PostListOutputDTO> userInfoListOutputDTOS = mPostMapper.postsToPostListOutputDTOs(postList);
        // 返回业务分页数据
        return success(userInfoListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody PostCreateInputDTO postCreateInputDTO) {
        Post post = mPostMapper.postCreateInputDTOToPost(postCreateInputDTO);
        return super.save(post);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody PostUpdateInputDTO postUpdateInputDTO) {
        Post post = mPostMapper.postUpdateInputDTOToPost(postUpdateInputDTO);
        return super.update(post);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "岗位id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "岗位id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }
}
