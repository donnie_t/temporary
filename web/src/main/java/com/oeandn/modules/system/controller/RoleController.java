package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.modules.system.dto.input.RoleCreateInputDTO;
import com.oeandn.modules.system.dto.input.RoleResourceInputDTO;
import com.oeandn.modules.system.dto.input.RoleSearchInputDTO;
import com.oeandn.modules.system.dto.input.RoleUpdateInputDTO;
import com.oeandn.modules.system.dto.output.RoleListOutputDTO;
import com.oeandn.modules.system.dtomapper.MRoleMapper;
import com.oeandn.modules.system.service.AuthService;
import com.oeandn.system.model.Role;
import com.oeandn.system.service.RoleService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/8/26 9:29
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/role")
@ApiSupport(order = 5)
@Api(value = "RoleController", tags = {"角色管理"})
public class RoleController extends BaseController<RoleService, Role> {

    private final MRoleMapper mRoleMapper;
    private final AuthService authService;

    @Log
    @ApiOperation(value = "角色分页查询")
    @GetMapping(value = "/page")
    public R<PageResult<RoleListOutputDTO>> page(RoleSearchInputDTO roleSearchInputDTO, PageModel pageModel) {
        // 转换model
        Role role = mRoleMapper.roleSearchInputDTOToRole(roleSearchInputDTO);
        // 获取源对象
        IPage<Role> pageListByEntity = baseService.pageListByEntity(role, pageModel);
        // 转换新对象
        List<RoleListOutputDTO> roleListOutputDTOS = mRoleMapper.rolesToRoleListOutputDTOs(pageListByEntity.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByEntity, roleListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "获取全部信息")
    @GetMapping(value = "/all")
    public R<List<RoleListOutputDTO>> getAllRoleList() {
        List<Role> roleList = baseService.list();
        List<RoleListOutputDTO> roleListOutputDTOS = mRoleMapper.rolesToRoleListOutputDTOs(roleList);
        return success(roleListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping(value = "/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody RoleCreateInputDTO roleCreateInputDTO) {
        Role role = mRoleMapper.roleCreateInputDTOToRole(roleCreateInputDTO);
        boolean bool = baseService.saveRole(role, roleCreateInputDTO.getDeptIds(), roleCreateInputDTO.getResIds());
        if (bool) {
            // 只能刷新当前登录人的权限，不能修改角色下所有用户的权限
            authService.refreshUserInfoByUserId(getUserId());
        }
        return success(bool);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody RoleUpdateInputDTO roleUpdateInputDTO) {
        Role role = mRoleMapper.roleUpdateInputDTOToRole(roleUpdateInputDTO);
        boolean bool = baseService.saveRole(role, roleUpdateInputDTO.getDeptIds(), roleUpdateInputDTO.getResIds());
        if (bool) {
            // 只能刷新当前登录人的权限，不能修改角色下所有用户的权限
            authService.refreshUserInfoByUserId(getUserId());
        }
        return success(bool);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "角色id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "角色id不能为空") @RequestParam Long id) {
        return success(baseService.deleteRole(id));
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<Long> idList) {
        return success(baseService.deleteBatchRole(idList));
    }


    @Log
    @ApiOperation(value = "获取已授权的数据范围部门ids")
    @ApiImplicitParam(name = "roleId", value = "角色Id", dataType = "Long", paramType = "query", required = true)
    @GetMapping(value = "/getDataScopeDeptIdsByRoleId")
    public R<List<Long>> getDataScopeDeptIdsByRoleId(@NotBlank(message = "角色id不能为空") @RequestParam Long roleId) {
        return success(baseService.getDataScopeDeptIdsByRoleId(roleId));
    }
}
