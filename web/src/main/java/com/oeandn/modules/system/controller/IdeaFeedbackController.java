package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.IdeaFeedbackCreateInputDTO;
import com.oeandn.modules.system.dto.input.IdeaFeedbackSearchInputDTO;
import com.oeandn.modules.system.dto.input.IdeaFeedbackUpdateInputDTO;
import com.oeandn.modules.system.dto.output.IdeaFeedbackListOutputDTO;
import com.oeandn.modules.system.dtomapper.MIdeaFeedbackMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.IdeaFeedback;
import com.oeandn.system.service.IdeaFeedbackService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 意见反馈 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/idea-feedback")
@Api(value = "IdeaFeedbackController", tags = {"意见反馈管理"})
public class IdeaFeedbackController extends BaseController<IdeaFeedbackService,IdeaFeedback> {

    private final MIdeaFeedbackMapper mIdeaFeedbackMapper;

    @Log
    @ApiOperation(value = "意见反馈分页查询")
    @GetMapping("/page")
    public R<PageResult<IdeaFeedbackListOutputDTO>> pageList(IdeaFeedbackSearchInputDTO ideaFeedbackSearchInputDTO, PageModel pageModel) {
        // 转换model
        IdeaFeedback ideaFeedback = mIdeaFeedbackMapper.toIdeaFeedback(ideaFeedbackSearchInputDTO);
        // 获取源对象
        IPage<IdeaFeedback> pageListByListInputDTO = baseService.pageListByEntity(ideaFeedback, pageModel);
        // 转换新对象
        List<IdeaFeedbackListOutputDTO> ideaFeedbackListOutputDTOS = mIdeaFeedbackMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, ideaFeedbackListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody IdeaFeedbackCreateInputDTO ideaFeedbackCreateInputDTO) {
        IdeaFeedback ideaFeedback = mIdeaFeedbackMapper.toIdeaFeedback(ideaFeedbackCreateInputDTO);
        return super.save(ideaFeedback);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody IdeaFeedbackUpdateInputDTO ideaFeedbackUpdateInputDTO) {
        IdeaFeedback ideaFeedback = mIdeaFeedbackMapper.toIdeaFeedback(ideaFeedbackUpdateInputDTO);
        return super.update(ideaFeedback);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

}
