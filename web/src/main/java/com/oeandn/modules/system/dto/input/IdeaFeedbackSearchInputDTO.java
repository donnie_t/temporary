package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 意见反馈
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="IdeaFeedbackSearchInputDTO", description="意见反馈查询表单")
public class IdeaFeedbackSearchInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "反馈类型")
    private String feedbackType;

    @ApiModelProperty(value = "反馈内容")
    private String feedbackContent;

    @ApiModelProperty(value = "开始时间")
    private Date startDate;
    @ApiModelProperty(value = "结束时间")
    private Date endDate;

}
