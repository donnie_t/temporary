package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 授权权限使用
 * @Date: 2020/8/26 9:44
 */
@Getter
@Setter
@ApiModel(value="RoleResourceInputDTO", description="授权权限使用")
public class RoleResourceInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "角色Id")
    private Long roleId;


}
