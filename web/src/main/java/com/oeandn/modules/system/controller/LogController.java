package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.LogSearchInputDTO;
import com.oeandn.modules.system.dto.output.LogListOutputDTO;
import com.oeandn.modules.system.dtomapper.MLogMapper;
import com.oeandn.system.model.Log;
import com.oeandn.system.service.LogService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 日志表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-01-06
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/log")
@ApiSupport(order = 35)
@Api(value = "LogController", tags = {"日志管理"})
public class LogController extends BaseController<LogService, Log> {

    private final MLogMapper mLogMapper;

    @ApiOperation(value = "日志分页查询")
    @GetMapping("/page")
    public R<PageResult<LogListOutputDTO>> pageList(LogSearchInputDTO logSearchInputDTO, PageModel pageModel) {
        // 转换model
        Log log = mLogMapper.logSearchInputDTOToLog(logSearchInputDTO);
        // 获取源对象
        IPage<Log> pageListByListInputDTO = baseService.pageListByEntity(log, pageModel);
        // 转换新对象
        List<LogListOutputDTO> userInfoListOutputDTOS = mLogMapper.logsToLogListOutputDTOs(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, userInfoListOutputDTOS));
    }
}
