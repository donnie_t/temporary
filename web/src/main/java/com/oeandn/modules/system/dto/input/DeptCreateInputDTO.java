package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-16
 */
@Getter
@Setter
@ApiModel(value = "DeptCreateInputDTO", description = "部门添加表单")
public class DeptCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父部门id")
    private Long parentId;

    @ApiModelProperty(value = "部门名称")
    private String name;

    @ApiModelProperty(value = "显示顺序")
    private BigDecimal deptSort;


    @ApiModelProperty(value = "部门状态（0、正常；1、停用）-字典")
    private String deptStatus;


}
