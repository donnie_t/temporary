package com.oeandn.modules.system.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.constant.Constant;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.modules.system.dto.input.ResourceCreateInputDTO;
import com.oeandn.modules.system.dto.input.ResourceUpdateInputDTO;
import com.oeandn.modules.system.dto.output.ResourceListOutputDTO;
import com.oeandn.modules.system.dto.output.RouterListOutputDTO;
import com.oeandn.modules.system.dtomapper.MResourceMapper;
import com.oeandn.system.enums.ResourceMenuHiddenFlagEnum;
import com.oeandn.system.enums.ResourcesTypeEnum;
import com.oeandn.system.model.Resource;
import com.oeandn.system.service.ResourceService;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/8/26 9:31
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/resource")
@ApiSupport(order = 10)
@Api(value = "ResourceController", tags = {"资源管理"})
public class ResourceController extends BaseController<ResourceService, Resource> {

    private final MResourceMapper mResourcesMapper;

    @Log
    @ApiOperation(value = "获取资源tree列表数据")
    @GetMapping(value = "/getResourceTreeList")
    public R<List<ResourceListOutputDTO>> getResourceTreeList() {
        List<Resource> resourceTreeTableList = baseService.getResourceTreeList();
        List<ResourceListOutputDTO> resourcesListOutputDTOS = mResourcesMapper.toOutputDTO(resourceTreeTableList);
        return success(resourcesListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "获取菜单列表数据")
    @GetMapping(value = "/getMenuList")
    public R<List<ResourceListOutputDTO>> getMenuList() {
        List<Resource> resourceTreeTableList = baseService.getMenuList();
        List<ResourceListOutputDTO> resourcesListOutputDTOS = mResourcesMapper.toOutputDTO(resourceTreeTableList);
        return success(resourcesListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "根据角色id获取资源tree列表数据")
    @ApiImplicitParam(name = "roleId", value = "角色id", dataType = "roleId", paramType = "query", required = true)
    @GetMapping(value = "/getResourceIdListByRoleId")
    public R<List<Long>> getResourceIdListByRoleId(@NotBlank(message = "角色id不能为空") @RequestParam Long roleId) {
        return success(baseService.getResourceIdListByRoleId(roleId));
    }

    @Log
    @ApiOperation(value = "根据登录人获取目录和菜单路由（vue使用）")
    @GetMapping(value = "/getRouterList")
    public R<List<RouterListOutputDTO>> getResourceList() {
        final Long userId = getUserId();
        final List<Resource> resourcesList = baseService.listByUserId(userId);
        return success(generateRouter(resourcesList));
    }

    private List<RouterListOutputDTO> generateRouter(List<Resource> resourcesList) {
        List<RouterListOutputDTO> routerListOutputDTOS = new ArrayList<>();
        resourcesList.forEach(resources -> {
            final List<Resource> children = resources.getChildren();
            RouterListOutputDTO routerListOutputDTO = new RouterListOutputDTO();
            routerListOutputDTO.setName(resources.getComponentName());
            routerListOutputDTO.setPath(resources.getRoutePath());

            if (Constant.COMMON_PARENT_ID.equals(resources.getParentId())) {
                routerListOutputDTO.setComponent(StrUtil.isBlank(resources.getComponentPath()) ? Constant.ROUTER_LAYOUT : resources.getComponentPath());
                // 如果不是一级菜单，并且菜单类型为目录，则代表是多级菜单
            } else if (ResourcesTypeEnum.CATALOGUE.code.equals(resources.getType())) {
                routerListOutputDTO.setComponent(StrUtil.isBlank(resources.getComponentPath()) ? Constant.ROUTER_PARENT_VIEW : resources.getComponentPath());
            } else if (StrUtil.isNotBlank(resources.getComponentPath())) {
                routerListOutputDTO.setComponent(resources.getComponentPath());
            }
            // 设置元信息
            final RouterListOutputDTO.MetaOutputDTO metaOutputDTO = new RouterListOutputDTO.MetaOutputDTO();
            metaOutputDTO.setIcon(resources.getMenuIcon());
            metaOutputDTO.setTitle(resources.getTitle());
            metaOutputDTO.setAlwaysShow(true);
            metaOutputDTO.setHidden(!ResourceMenuHiddenFlagEnum.YES.code.equals(resources.getMenuHiddenFlag()));
            routerListOutputDTO.setMeta(metaOutputDTO);
            if (CollUtil.isNotEmpty(children)) {
                routerListOutputDTO.setRedirect(Constant.ROUTER_NO_REDIRECT);
                routerListOutputDTO.setChildren(generateRouter(children));
            } else if (Constant.COMMON_PARENT_ID.equals(resources.getParentId())) {
                RouterListOutputDTO routerListOutputDTONew = new RouterListOutputDTO();
                routerListOutputDTONew.setMeta(routerListOutputDTO.getMeta());
                routerListOutputDTONew.setName(resources.getComponentName());
                routerListOutputDTONew.setComponent(resources.getComponentPath());
                routerListOutputDTONew.setPath(resources.getRoutePath());

                routerListOutputDTO.setName(null);
                routerListOutputDTO.setComponent(Constant.ROUTER_LAYOUT);
                List<RouterListOutputDTO> listOutputDTOS = new ArrayList<>();
                listOutputDTOS.add(routerListOutputDTONew);
                routerListOutputDTO.setChildren(listOutputDTOS);
            }
            routerListOutputDTOS.add(routerListOutputDTO);
        });
        return routerListOutputDTOS;
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping(value = "/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ResourceCreateInputDTO resourcesCreateInputDTO) {
        Resource resources = mResourcesMapper.toResource(resourcesCreateInputDTO);
        resources.setCreateDate(new Date());
        return success(baseService.saveOrUpdate(resources));
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ResourceUpdateInputDTO updateResourcesInputDTO) {
        Resource resources = mResourcesMapper.toResource(updateResourcesInputDTO);
        return success(baseService.saveOrUpdate(resources));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "资源id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "资源id不能为空") @RequestParam Long id) {
        return success(baseService.removeById(id));
    }
}
