package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.DangerZoneCreateInputDTO;
import com.oeandn.modules.system.dto.input.DangerZoneSearchInputDTO;
import com.oeandn.modules.system.dto.input.DangerZoneUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DangerZoneListOutputDTO;
import com.oeandn.system.model.DangerZone;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 危险区-红橙区 转换类
 * @Date: 2022-08-10
 */
@Mapper(componentModel = "spring")
public interface MDangerZoneMapper {

    DangerZone toDangerZone(DangerZoneSearchInputDTO dangerZoneSearchInputDTO);

    DangerZone toDangerZone(DangerZoneCreateInputDTO dangerZoneCreateInputDTO);

    DangerZone toDangerZone(DangerZoneUpdateInputDTO dangerZoneUpdateInputDTO);

    DangerZoneListOutputDTO toOutput(DangerZone dangerZone);

    List<DangerZoneListOutputDTO> toOutputList(List<DangerZone> dangerZones);
}