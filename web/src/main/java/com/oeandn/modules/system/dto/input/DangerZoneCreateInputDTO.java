package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 危险区-红橙区
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="DangerZoneCreateInputDTO", description="危险区-红橙区创建表单")
public class DangerZoneCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "红橙区名称")
    private String zoneName;

    @ApiModelProperty(value = "房间号")
    private String roomNo;

    @ApiModelProperty(value = "坐标1")
    private String coordOne;

    @ApiModelProperty(value = "坐标2")
    private String coordTwo;

    @ApiModelProperty(value = "坐标3")
    private String coordThree;

    @ApiModelProperty(value = "坐标4")
    private String coordFour;

    @ApiModelProperty(value = "1橙区2红区")
    private String zoneTag;


}
