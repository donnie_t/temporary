package com.oeandn.modules.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.system.dto.input.DangerZoneCreateInputDTO;
import com.oeandn.modules.system.dto.input.DangerZoneSearchInputDTO;
import com.oeandn.modules.system.dto.input.DangerZoneUpdateInputDTO;
import com.oeandn.modules.system.dto.output.DangerZoneListOutputDTO;
import com.oeandn.modules.system.dtomapper.MDangerZoneMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.DangerZone;
import com.oeandn.system.service.DangerZoneService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 危险区-红橙区 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/danger-zone")
@Api(value = "DangerZoneController", tags = {"危险区-红橙区管理"})
public class DangerZoneController extends BaseController<DangerZoneService,DangerZone> {

    private final MDangerZoneMapper mDangerZoneMapper;

    @Log
    @ApiOperation(value = "危险区-红橙区分页查询")
    @GetMapping("/page")
    public R<PageResult<DangerZoneListOutputDTO>> pageList(DangerZoneSearchInputDTO dangerZoneSearchInputDTO, PageModel pageModel) {
        // 转换model
        DangerZone dangerZone = mDangerZoneMapper.toDangerZone(dangerZoneSearchInputDTO);
        // 获取源对象
        IPage<DangerZone> pageListByListInputDTO = baseService.pageListByEntity(dangerZone, pageModel);
        // 转换新对象
        List<DangerZoneListOutputDTO> dangerZoneListOutputDTOS = mDangerZoneMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, dangerZoneListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody DangerZoneCreateInputDTO dangerZoneCreateInputDTO) {
        DangerZone dangerZone = mDangerZoneMapper.toDangerZone(dangerZoneCreateInputDTO);
        return super.save(dangerZone);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody DangerZoneUpdateInputDTO dangerZoneUpdateInputDTO) {
        DangerZone dangerZone = mDangerZoneMapper.toDangerZone(dangerZoneUpdateInputDTO);
        return super.update(dangerZone);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

}
