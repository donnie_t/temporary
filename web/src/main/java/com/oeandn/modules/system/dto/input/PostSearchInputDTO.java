package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 岗位表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-16
 */
@Getter
@Setter
@ApiModel(value = "PostSearchInputDTO对象", description = "岗位表格查询参数")
public class PostSearchInputDTO {

    @ApiModelProperty(value = "岗位名称")
    private String postName;

    @ApiModelProperty(value = "岗位状态（0、正常；1、停用）-字典")
 private String postStatus;

}
