package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.ResourceCreateInputDTO;
import com.oeandn.modules.system.dto.input.ResourceUpdateInputDTO;
import com.oeandn.modules.system.dto.output.ResourceListOutputDTO;
import com.oeandn.system.model.Resource;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper(componentModel = "spring")
public interface MResourceMapper {

    /**
     * 创建DTO 转换 resource
     *
     * @param resourceCreateInputDTO
     * @return
     */
    Resource toResource(ResourceCreateInputDTO resourceCreateInputDTO);

    /**
     * 修改DTO 转换 resource
     *
     * @param resourceUpdateInputDTO
     * @return
     */
    Resource toResource(ResourceUpdateInputDTO resourceUpdateInputDTO);

    ResourceListOutputDTO toOutputDTO(Resource resource);

    List<ResourceListOutputDTO> toOutputDTO(List<Resource> resourceList);
}
