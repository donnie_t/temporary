package com.oeandn.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 字典类型表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Getter
@Setter
@ApiModel(value = "DictTypeSearchInputDTO", description = "字典类型查询使用")
public class DictTypeSearchInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典名称")
    private String dictName;

    @ApiModelProperty(value = "状态（0正常 1停用）-字典")
    private String dictStatus;

}
