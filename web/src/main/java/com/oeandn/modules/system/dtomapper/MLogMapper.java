package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.*;
import com.oeandn.modules.system.dto.output.LogListOutputDTO;
import com.oeandn.modules.system.dto.output.LoginOutputDTO;
import com.oeandn.modules.system.dto.output.UserInfoListOutputDTO;
import com.oeandn.system.model.Log;
import com.oeandn.system.model.UserInfo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper(componentModel = "spring")
public interface MLogMapper {

    Log logSearchInputDTOToLog(LogSearchInputDTO logSearchInputDTO);

    LogListOutputDTO logToLogListOutputDTO(Log log);

    List<LogListOutputDTO> logsToLogListOutputDTOs(List<Log> logs);


}
