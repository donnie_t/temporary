package com.oeandn.modules.system.dtomapper;

import com.oeandn.modules.system.dto.input.IdeaFeedbackCreateInputDTO;
import com.oeandn.modules.system.dto.input.IdeaFeedbackSearchInputDTO;
import com.oeandn.modules.system.dto.input.IdeaFeedbackUpdateInputDTO;
import com.oeandn.modules.system.dto.output.IdeaFeedbackListOutputDTO;
import com.oeandn.system.model.IdeaFeedback;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 意见反馈 转换类
 * @Date: 2022-08-10
 */
@Mapper(componentModel = "spring")
public interface MIdeaFeedbackMapper {

    IdeaFeedback toIdeaFeedback(IdeaFeedbackSearchInputDTO ideaFeedbackSearchInputDTO);

    IdeaFeedback toIdeaFeedback(IdeaFeedbackCreateInputDTO ideaFeedbackCreateInputDTO);

    IdeaFeedback toIdeaFeedback(IdeaFeedbackUpdateInputDTO ideaFeedbackUpdateInputDTO);

    IdeaFeedbackListOutputDTO toOutput(IdeaFeedback ideaFeedback);

    List<IdeaFeedbackListOutputDTO> toOutputList(List<IdeaFeedback> ideaFeedbacks);
}