package com.oeandn.modules.inspect.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 巡检点
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="InspectPointSearchInputDTO", description="巡检点查询表单")
public class InspectPointSearchInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父巡检点id")
    private Long parentId;

    @ApiModelProperty(value = "1.厂房 2房间 3设备")
    private String inspectPointType;

    @ApiModelProperty(value = "巡检点名称，厂房名称，房间号，设备代码")
    private String inspectPointName;

    @ApiModelProperty(value = "所属机组")
    private String subordinateUnits;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "设备类型")
    private String deviceType;

    @ApiModelProperty(value = "设备负责人")
    private Long deviceTakeChargeBy;

    @ApiModelProperty(value = "设备负责人")
    private String deviceTakeChargeName;

    @ApiModelProperty(value = "设备标签id")
    private String deviceLebelId;

    @ApiModelProperty(value = "设备标签名称")
    private String deviceLebelName;

    @ApiModelProperty(value = "设备状态--字典  1正常 2异常 3 停产")
    private String deviceStatus;

    @ApiModelProperty(value = "附件地址")
    private String accessory;

    @ApiModelProperty(value = "坐标")
    private String coord;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "部门id")
    private Long deptId;



}
