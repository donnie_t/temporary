package com.oeandn.modules.inspect.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 巡检审批表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="ApproveRecordSearchInputDTO", description="巡检审批表查询表单")
public class ApproveRecordSearchInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门id")
    private Long deptId;

    @ApiModelProperty(value = "业务主键id")
    private Long businessId;

    @ApiModelProperty(value = "业务类型1巡检路线2巡检点3巡检记录4巡检项目")
    private String businessType;

    @ApiModelProperty(value = "审批人id")
    private Long approveUserId;

    @ApiModelProperty(value = "审批状态（0-待审批，1-审批通过，2-审批拒绝）")
    private String approveState;

    @ApiModelProperty(value = "创建者")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新者")
    private Long updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "备注")
    private String remark;

}
