package com.oeandn.modules.inspect.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.modules.inspect.dto.input.InspectPointItemCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointItemSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointItemUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPointItemListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MInspectPointItemMapper;
import com.oeandn.system.entity.InspectPointItemDetailDTO;
import com.oeandn.system.model.InspectPointItem;
import com.oeandn.system.service.InspectPointItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 巡检项 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/inspect-point-item")
@Api(value = "InspectPointItemController", tags = {"巡检项管理"})
public class InspectPointItemController extends BaseController<InspectPointItemService, InspectPointItem> {

    private final MInspectPointItemMapper mInspectPointItemMapper;

    @Log
    @ApiOperation(value = "巡检项分页查询")
    @GetMapping("/page")
    public R<PageResult<InspectPointItemListOutputDTO>> pageList(InspectPointItemSearchInputDTO inspectPointItemSearchInputDTO, PageModel pageModel) {
        // 转换model
        InspectPointItem inspectPointItem = mInspectPointItemMapper.toInspectPointItem(inspectPointItemSearchInputDTO);
        // 获取源对象
        IPage<InspectPointItem> pageListByListInputDTO = baseService.pageListByEntity(inspectPointItem, pageModel);
        // 转换新对象
        List<InspectPointItemListOutputDTO> inspectPointItemListOutputDTOS =
                mInspectPointItemMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, inspectPointItemListOutputDTOS));
    }

    //@PathVariable("id")
    @Log
    @ApiOperation(value = "巡检项详情查询")
    @GetMapping("/getdetail/{id}")
    public R<InspectPointItemDetailDTO> getdetail(Long id) {

        return R.success(baseService.getdetail(id));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody InspectPointItemCreateInputDTO inspectPointItemCreateInputDTO) {
        InspectPointItem inspectPointItem = mInspectPointItemMapper.toInspectPointItem(inspectPointItemCreateInputDTO);
        return R.success(baseService.saveInspectPointItem(inspectPointItem));

    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody InspectPointItemUpdateInputDTO inspectPointItemUpdateInputDTO) {
        InspectPointItem inspectPointItem = mInspectPointItemMapper.toInspectPointItem(inspectPointItemUpdateInputDTO);
        return R.success(baseService.updateInspectPointItem(inspectPointItem));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

}
