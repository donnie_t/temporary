package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.InspectRecordItemCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectRecordItemListOutputDTO;
import com.oeandn.system.model.InspectRecordItem;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检记录巡检项表 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MInspectRecordItemMapper {

    InspectRecordItem toInspectRecordItem(InspectRecordItemSearchInputDTO inspectRecordItemSearchInputDTO);

    InspectRecordItem toInspectRecordItem(InspectRecordItemCreateInputDTO inspectRecordItemCreateInputDTO);

    InspectRecordItem toInspectRecordItem(InspectRecordItemUpdateInputDTO inspectRecordItemUpdateInputDTO);

    InspectRecordItemListOutputDTO toOutput(InspectRecordItem inspectRecordItem);

    List<InspectRecordItemListOutputDTO> toOutputList(List<InspectRecordItem> inspectRecordItems);
}