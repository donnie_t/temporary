package com.oeandn.modules.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.common.model.R;
import com.oeandn.db.base.controller.BaseController;
import com.oeandn.modules.inspect.dto.input.InspectPointCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPointListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MInspectPointMapper;
import com.oeandn.system.model.InspectPoint;
import com.oeandn.system.model.InspectPointItem;
import com.oeandn.system.service.InspectPointItemService;
import com.oeandn.system.service.InspectPointService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 巡检点 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/inspect-point")
@Api(value = "InspectPointController", tags = {"巡检点管理"})
public class InspectPointController extends BaseController<InspectPointService, InspectPoint> {

    private final MInspectPointMapper mInspectPointMapper;

    private final InspectPointItemService inspectPointItemService;

    @Log
    @ApiOperation(value = "巡检点分页查询")
    @GetMapping("/page")
    public R<PageResult<InspectPointListOutputDTO>> pageList(InspectPointSearchInputDTO inspectPointSearchInputDTO,
                                                             PageModel pageModel) {
        // 转换model
        InspectPoint inspectPoint = mInspectPointMapper.toInspectPoint(inspectPointSearchInputDTO);
        // 获取源对象
        IPage<InspectPoint> pageListByListInputDTO = baseService.pageListByEntity(inspectPoint, pageModel);
        // 转换新对象
        //List<InspectPointListOutputDTO> inspectPointListOutputDTOS = mInspectPointMapper.toOutputList
        // (setListUserName(pageListByListInputDTO.getRecords()));
        List<InspectPointListOutputDTO> inspectPointListOutputDTOS =
                mInspectPointMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, inspectPointListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "巡检点树形查询")
    @GetMapping("/getPointTreeList")
    public R<List<InspectPointListOutputDTO>> getPointTreeList() {

        List<InspectPoint> deptTreeTableList = baseService.getPointTreeList();

        List<InspectPointListOutputDTO> inspectPointListOutputDTOS =
                mInspectPointMapper.toOutputList(deptTreeTableList);

        return success(inspectPointListOutputDTOS);
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody InspectPointCreateInputDTO inspectPointCreateInputDTO) {
        InspectPoint inspectPoint = mInspectPointMapper.toInspectPoint(inspectPointCreateInputDTO);
        return R.success(baseService.saveInspectPoint(inspectPoint));
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody InspectPointUpdateInputDTO inspectPointUpdateInputDTO) {
        InspectPoint inspectPoint = mInspectPointMapper.toInspectPoint(inspectPointUpdateInputDTO);
        return R.success(baseService.updateInspectPoint(inspectPoint));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {

        long count = this.baseService.count(new QueryWrapper<InspectPoint>().eq("parent_id", id));
        // 巡检项
        long count1 = inspectPointItemService.count(new QueryWrapper<InspectPointItem>().eq(
                "inspect_point_id", id));

        if (0 < count) {
            return R.error("关联巡检点数据中");
        }
        if (0 < count1) {
            return R.error("关联巡检项数据中");
        }
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

}
