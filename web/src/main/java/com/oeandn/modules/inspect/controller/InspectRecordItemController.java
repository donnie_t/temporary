package com.oeandn.modules.inspect.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectRecordItemListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MInspectRecordItemMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.InspectRecordItem;
import com.oeandn.system.service.InspectRecordItemService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 巡检记录巡检项表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/inspect-record-item")
@Api(value = "InspectRecordItemController", tags = {"巡检记录巡检项管理"})
public class InspectRecordItemController extends BaseController<InspectRecordItemService,InspectRecordItem> {

    private final MInspectRecordItemMapper mInspectRecordItemMapper;

    @Log
    @ApiOperation(value = "巡检记录巡检项表分页查询")
    @GetMapping("/page")
    public R<PageResult<InspectRecordItemListOutputDTO>> pageList(InspectRecordItemSearchInputDTO inspectRecordItemSearchInputDTO, PageModel pageModel) {
        // 转换model
        InspectRecordItem inspectRecordItem = mInspectRecordItemMapper.toInspectRecordItem(inspectRecordItemSearchInputDTO);
        // 获取源对象
        IPage<InspectRecordItem> pageListByListInputDTO = baseService.pageListByEntity(inspectRecordItem, pageModel);
        // 转换新对象
        List<InspectRecordItemListOutputDTO> inspectRecordItemListOutputDTOS = mInspectRecordItemMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, inspectRecordItemListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody InspectRecordItemCreateInputDTO inspectRecordItemCreateInputDTO) {
        InspectRecordItem inspectRecordItem = mInspectRecordItemMapper.toInspectRecordItem(inspectRecordItemCreateInputDTO);
        return super.save(inspectRecordItem);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody InspectRecordItemUpdateInputDTO inspectRecordItemUpdateInputDTO) {
        InspectRecordItem inspectRecordItem = mInspectRecordItemMapper.toInspectRecordItem(inspectRecordItemUpdateInputDTO);
        return super.update(inspectRecordItem);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

    @Log
    @ApiOperation(value = "H5巡检完成批量插入")
    @PostMapping("/saveOrUpdate")
    public R<Boolean> saveOrUpdate(@Validated @ApiParam @RequestBody List<InspectRecordItem> InspectRecordItemList,String recordId,String dataComplete){
        return R.success(baseService.saveOrUpdate(InspectRecordItemList,recordId,dataComplete));
    }
}
