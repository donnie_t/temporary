package com.oeandn.modules.inspect.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 巡检项
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="InspectPointItemSearchInputDTO", description="巡检项查询表单")
public class InspectPointItemSearchInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "巡检点id")
    private Long inspectPointId;

    @ApiModelProperty(value = "巡检位置")
    private String inspectPlace;

    @ApiModelProperty(value = "设备码")
    private String deviceCode;

    @ApiModelProperty(value = "巡检名称")
    private String itemName;

    @ApiModelProperty(value = "属性code ")
    private String propertyCode;

    @ApiModelProperty(value = "属性名称")
    private String propertyName;

    @ApiModelProperty(value = "上限")
    private String upperLimit;

    @ApiModelProperty(value = "下限")
    private String lowerLimit;

    @ApiModelProperty(value = "高预警")
    private String highAlert;

    @ApiModelProperty(value = "低预警")
    private String lowAlert;

    @ApiModelProperty(value = "典型值")
    private String typicalValue;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @ApiModelProperty(value = "异常偏离值")
    private String abnormalDeviationValue;

    @ApiModelProperty(value = "创建者")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新者")
    private Long updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否历史记录0否1是")
    private String historyCode;

    @ApiModelProperty(value = "历史记录id")
    private Long historyId;

}
