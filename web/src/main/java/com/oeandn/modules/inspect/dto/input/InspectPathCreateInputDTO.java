package com.oeandn.modules.inspect.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 巡检路线
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="InspectPathCreateInputDTO", description="巡检路线创建表单")
public class InspectPathCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "路线类型-字典    1.日常巡检 2.大修巡检")
    private String pathType;

    @ApiModelProperty(value = "巡检路线名称")
    private String pathName;

    @ApiModelProperty(value = "巡检路线编号")
    private String pathCode;

    @ApiModelProperty(value = "巡检周期")
    private String inspectCycle;

    @ApiModelProperty(value = "巡检班值")
    private String inspectOnDuty;

    @ApiModelProperty(value = "次序")
    private String sequence;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "概括")
    private String summarize;

    @ApiModelProperty(value = "巡检项ids")
    private List<Long> ids;


}
