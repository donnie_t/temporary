package com.oeandn.modules.inspect.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.db.base.util.ViewMapper;
import com.oeandn.modules.inspect.dto.input.ApproveRecordCreateInputDTO;
import com.oeandn.common.utils.DaysUtil;
import com.oeandn.modules.inspect.dto.input.ApproveRecordSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.ApproveRecordUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.ApproveRecordListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MApproveRecordMapper;
import com.oeandn.system.entity.ApproveRevisionEntity;
import com.oeandn.system.entity.InspectPointApproveListDTO;
import com.oeandn.system.entity.PointApproveSelectDTO;
import com.oeandn.system.entity.InspectResultRecordEntity;
import com.oeandn.system.model.UserInfo;
import com.oeandn.system.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.ApproveRecord;
import com.oeandn.system.service.ApproveRecordService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 巡检审批表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/approve-record")
@Api(value = "ApproveRecordController", tags = {"巡检审批管理"})
public class ApproveRecordController extends BaseController<ApproveRecordService,ApproveRecord> {

    private final MApproveRecordMapper mApproveRecordMapper;
    private final UserInfoService userInfoService;

    @Log
    @ApiOperation(value = "巡检审批表分页查询")
    @GetMapping("/page")
    public R<PageResult<ApproveRecordListOutputDTO>> pageList(ApproveRecordSearchInputDTO approveRecordSearchInputDTO, PageModel pageModel) {
        // 转换model
        ApproveRecord approveRecord = mApproveRecordMapper.toApproveRecord(approveRecordSearchInputDTO);
        // 获取源对象
        IPage<ApproveRecord> pageListByListInputDTO = baseService.pageListByEntity(approveRecord, pageModel);
        // 转换新对象
        List<ApproveRecordListOutputDTO> approveRecordListOutputDTOS = mApproveRecordMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, approveRecordListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ApproveRecordCreateInputDTO approveRecordCreateInputDTO) {
        ApproveRecord approveRecord = mApproveRecordMapper.toApproveRecord(approveRecordCreateInputDTO);
        return super.save(approveRecord);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ApproveRecordUpdateInputDTO approveRecordUpdateInputDTO) {
        ApproveRecord approveRecord = mApproveRecordMapper.toApproveRecord(approveRecordUpdateInputDTO);
        return super.update(approveRecord);
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

    @Log
    @ApiOperation(value = "巡检结果审批多表分页查询")
    @GetMapping("/getResultList")
    public R<PageResult<InspectResultRecordEntity>> getResultList(InspectResultRecordEntity inspectResultRecordEntity, PageModel pageModel) throws  ParseException {
        PageResult result =new PageResult();
        // 获取源对象
        Page<InspectResultRecordEntity> inspectResulRecord = baseService.pageRecord(inspectResultRecordEntity, pageModel);
        result.setCurrent(inspectResulRecord.getCurrent());
        result.setPages(inspectResulRecord.getPages());
        result.setSize(inspectResulRecord.getSize());
        result.setTotal(inspectResulRecord.getTotal());
        List<InspectResultRecordEntity> list = inspectResulRecord.getRecords();
        for(int i = 0,n = list.size();i < n;i++) {
            InspectResultRecordEntity record = inspectResulRecord.getRecords().get(i);
            record.setInspectdate(DaysUtil.dayBetween(record.getStartdate(),record.getEnddate()));
            list.set(i,record);
        }
        result.setRecords(list);
        // 返回业务分页数据
        return success(result);
    }


    @Log
    @ApiOperation(value = "审批管理-巡检点审批分页查询")
    @GetMapping("/pointPage")
    public R<PageResult<InspectPointApproveListDTO>> pointPage(PointApproveSelectDTO dto) {
        return success(baseService.pointPage(dto));
    }


    @Log
    @ApiOperation(value = "审批管理-巡检审批")
    @PostMapping("/approve")
    public R<Boolean> approve(@Validated @ApiParam @RequestBody ApproveRecordCreateInputDTO approveRecordApproveInputDTO){

        ApproveRecord approveRecord = mApproveRecordMapper.toApproveRecord(approveRecordApproveInputDTO);

        return R.success(baseService.approve(approveRecord));
    }

    @Log
    @ApiOperation(value = "审批管理-巡检路线修订")
    @GetMapping("/approveReisionPage")
    public R<PageResult<ApproveRevisionEntity>> approveReisionPage(ApproveRevisionEntity approveRevisionsEntity, PageModel pageModel) {
        PageResult result =new PageResult();
        // 获取源对象
        Page<ApproveRevisionEntity> approveRevision = baseService.approveReisionPage(approveRevisionsEntity, pageModel);
        result.setCurrent(approveRevision.getCurrent());
        result.setPages(approveRevision.getPages());
        result.setSize(approveRevision.getSize());
        result.setTotal(approveRevision.getTotal());
        List<ApproveRevisionEntity> list = approveRevision.getRecords();
        //表关联根据用户id查询用户名称
        ViewMapper.batchMapView(list,
                ViewMapper.setView(this::getMapUserInfo,ApproveRevisionEntity:: getUpdateBy,
                        ((approveRevisionEntity, userinfo) -> approveRevisionEntity.setUpdateName(userinfo.getUsername()))));
        ViewMapper.batchMapView(list,
                ViewMapper.setView(
                        this::getMapUserInfo,
                        ApproveRevisionEntity:: getApproveUserId,
                        ((approveRevisionEntity, userinfo) -> approveRevisionEntity.setApproveUserName(userinfo.getUsername()))));

        result.setRecords(list);
        // 返回业务分页数据
        return success(result);
    }
    private Map<Long, UserInfo> getMapUserInfo(Collection<Long> ids) {
        return userInfoService.listByIds(ids)
                .stream().collect(Collectors.toMap(UserInfo::getId, userinfo -> userinfo));
    }
}
