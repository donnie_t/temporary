package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.ApproveRecordCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.ApproveRecordSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.ApproveRecordUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.ApproveRecordListOutputDTO;
import com.oeandn.system.model.ApproveRecord;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检审批表 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MApproveRecordMapper {

    ApproveRecord toApproveRecord(ApproveRecordSearchInputDTO approveRecordSearchInputDTO);

    ApproveRecord toApproveRecord(ApproveRecordCreateInputDTO approveRecordCreateInputDTO);

    ApproveRecord toApproveRecord(ApproveRecordUpdateInputDTO approveRecordUpdateInputDTO);

    ApproveRecordListOutputDTO toOutput(ApproveRecord approveRecord);

    List<ApproveRecordListOutputDTO> toOutputList(List<ApproveRecord> approveRecords);
}