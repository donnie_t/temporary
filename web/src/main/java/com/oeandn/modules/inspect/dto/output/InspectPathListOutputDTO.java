package com.oeandn.modules.inspect.dto.output;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 巡检路线
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value = "InspectPathListOutputDTO", description = "巡检路线列表")
public class InspectPathListOutputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "表主键")
    private Long id;

    @ApiModelProperty(value = "路线类型-字典    1.日常巡检 2.大修巡检")
    private String pathType;

    @ApiModelProperty(value = "巡检路线名称")
    private String pathName;

    @ApiModelProperty(value = "巡检路线编号")
    private String pathCode;

    @ApiModelProperty(value = "巡检周期")
    private String inspectCycle;

    @ApiModelProperty(value = "巡检班值")
    private String inspectOnDuty;

    @ApiModelProperty(value = "次序")
    private String sequence;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "概括")
    private String summarize;


    @ApiModelProperty(value = "创建者")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新者")
    private Long updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否历史记录0否1是")
    private String historyCode;

    @ApiModelProperty(value = "历史记录id")
    private Long historyId;

    /**
     * 路线状态    默认0.启用 1.停用
     */
    @ApiModelProperty(value = "路线状态    默认0.启用 1.停用")
    private String pathStatus;

    /**
     * 审批状态（0-待审批，1-审批通过，2-审批拒绝）
     */
    @ApiModelProperty(value = "审批状态（0-待审批，1-审批通过，2-审批拒绝）")
    @TableField(exist = false)
    private String approveState;
}
