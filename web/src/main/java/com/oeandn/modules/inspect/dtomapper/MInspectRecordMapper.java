package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.InspectRecordCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectRecordListOutputDTO;
import com.oeandn.system.model.InspectRecord;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检记录表 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MInspectRecordMapper {

    InspectRecord toInspectRecord(InspectRecordSearchInputDTO inspectRecordSearchInputDTO);

    InspectRecord toInspectRecord(InspectRecordCreateInputDTO inspectRecordCreateInputDTO);

    InspectRecord toInspectRecord(InspectRecordUpdateInputDTO inspectRecordUpdateInputDTO);

    InspectRecordListOutputDTO toOutput(InspectRecord inspectRecord);

    List<InspectRecordListOutputDTO> toOutputList(List<InspectRecord> inspectRecords);
}