package com.oeandn.modules.inspect.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.modules.inspect.dto.input.InspectPathCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPathSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPathUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPathListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MInspectPathMapper;
import com.oeandn.system.entity.InspectPathDetailDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.InspectPath;
import com.oeandn.system.service.InspectPathService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 巡检路线 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/inspect-path")
@Api(value = "InspectPathController", tags = {"巡检路线管理"})
public class InspectPathController extends BaseController<InspectPathService,InspectPath> {

    private final MInspectPathMapper mInspectPathMapper;

    @Log
    @ApiOperation(value = "巡检路线分页查询")
    @GetMapping("/page")
    public R<PageResult<InspectPathListOutputDTO>> pageList(InspectPathSearchInputDTO inspectPathSearchInputDTO, PageModel pageModel) {
        // 转换model
        InspectPath inspectPath = mInspectPathMapper.toInspectPath(inspectPathSearchInputDTO);
        // 获取源对象
        IPage<InspectPath> pageListByListInputDTO = baseService.pageListByEntity(inspectPath, pageModel);
        // 转换新对象
        List<InspectPathListOutputDTO> inspectPathListOutputDTOS = mInspectPathMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, inspectPathListOutputDTOS));
    }

    @Log
    @ApiOperation(value = "巡检路线详情查询")
    @GetMapping("/getdetail/{id}")
    public R<InspectPathDetailDTO> getdetail(@PathVariable("id") Long id){

       return R.success(baseService.getdetail(id));
    }

    @Log
    @ApiOperation(value = "巡检路线修改启停状态")
    @GetMapping("/start-stop/{id}")
    public R<Boolean> startAndStop(@PathVariable("id") Long id){

        return R.success(baseService.startAndStop(id));
    }


    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody InspectPathCreateInputDTO inspectPathCreateInputDTO) {
        InspectPath inspectPath = mInspectPathMapper.toInspectPath(inspectPathCreateInputDTO);
        return R.success(baseService.saveInspectPath(inspectPath));
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody InspectPathUpdateInputDTO inspectPathUpdateInputDTO) {
        InspectPath inspectPath = mInspectPathMapper.toInspectPath(inspectPathUpdateInputDTO);

        return R.success(baseService.updateInspectPath(inspectPath));
    }

    @Log
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "Long", paramType = "query", required = true)
    @DeleteMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        return super.delete(id);
    }

    @Log
    @ApiOperation(value = "根据list删除")
    @DeleteMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return super.deleteBatch(idList);
    }

}
