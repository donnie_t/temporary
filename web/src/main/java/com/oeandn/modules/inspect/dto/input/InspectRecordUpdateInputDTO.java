package com.oeandn.modules.inspect.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 巡检记录表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value="InspectRecordUpdateInputDTO", description="巡检记录表修改表单")
public class InspectRecordUpdateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "路线id")
    private Long pathId;

    @ApiModelProperty(value = "0空缺1完整")
    private String dataComplete;

    @ApiModelProperty(value = "审批id")
    private Long approveId;

    @ApiModelProperty(value = "创建者")
    private Long createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "任务创建时间")
    private Date taskCreateDate;

    @ApiModelProperty(value = "更新者")
    private Long updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "部门Id")
    private String deptId;

    @ApiModelProperty(value = "巡检开始时间")
    private String startDate;

    @ApiModelProperty(value = "巡检结束时间")
    private String endDate;

    @ApiModelProperty(value = "巡检状态1未开始巡检2巡检进行中3巡检已完成")
    private String status;

    @ApiModelProperty(value = "备注")
    private String remark;

}
