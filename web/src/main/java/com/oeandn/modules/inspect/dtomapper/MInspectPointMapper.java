package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.InspectPointCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPointListOutputDTO;
import com.oeandn.system.model.InspectPoint;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检点 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MInspectPointMapper {

    InspectPoint toInspectPoint(InspectPointSearchInputDTO inspectPointSearchInputDTO);

    InspectPoint toInspectPoint(InspectPointCreateInputDTO inspectPointCreateInputDTO);

    InspectPoint toInspectPoint(InspectPointUpdateInputDTO inspectPointUpdateInputDTO);

    InspectPointListOutputDTO toOutput(InspectPoint inspectPoint);

    List<InspectPointListOutputDTO> toOutputList(List<InspectPoint> inspectPoints);
}