package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.InspectPointItemCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointItemSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPointItemUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPointItemListOutputDTO;
import com.oeandn.system.model.InspectPointItem;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检项 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MInspectPointItemMapper {

    InspectPointItem toInspectPointItem(InspectPointItemSearchInputDTO inspectPointItemSearchInputDTO);

    InspectPointItem toInspectPointItem(InspectPointItemCreateInputDTO inspectPointItemCreateInputDTO);

    InspectPointItem toInspectPointItem(InspectPointItemUpdateInputDTO inspectPointItemUpdateInputDTO);

    InspectPointItemListOutputDTO toOutput(InspectPointItem inspectPointItem);

    List<InspectPointItemListOutputDTO> toOutputList(List<InspectPointItem> inspectPointItems);
}