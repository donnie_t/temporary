package com.oeandn.modules.inspect.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.common.annotation.Log;
import com.oeandn.common.model.R;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.common.utils.DaysUtil;
import com.oeandn.modules.inspect.dto.input.InspectRecordCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordItemSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectRecordUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectRecordItemListOutputDTO;
import com.oeandn.modules.inspect.dtomapper.MInspectRecordItemMapper;
import com.oeandn.modules.inspect.dtomapper.MInspectRecordMapper;
import com.oeandn.system.entity.ApproveRecordEntity;
import com.oeandn.system.entity.InspectRecordEntity;
import com.oeandn.system.model.ApproveRecord;
import com.oeandn.system.model.InspectRecordItem;
import com.oeandn.system.service.ApproveRecordService;
import com.oeandn.system.service.InspectRecordItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import com.oeandn.system.model.InspectRecord;
import com.oeandn.system.service.InspectRecordService;
import org.springframework.web.bind.annotation.*;
import com.oeandn.db.base.controller.BaseController;

import javax.validation.constraints.NotBlank;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 巡检记录表 前端控制器
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/inspect-record")
@Api(value = "InspectRecordController", tags = {"巡检记录管理"})
public class InspectRecordController extends BaseController<InspectRecordService,InspectRecord> {

    private final MInspectRecordMapper mInspectRecordMapper;
    private final MInspectRecordItemMapper mInspectRecordItemMapper;
    private final InspectRecordItemService inspectRecordItemService;
    private final ApproveRecordService approveRecordService;

    @Log
    @ApiOperation(value = "巡检记录表多表分页查询")
    @GetMapping("/pages")
    public R<PageResult<InspectRecordEntity>> pagesList(InspectRecordEntity inspectRecordEntity, PageModel pageModel) throws ParseException {
        PageResult result =new PageResult();
        // 获取源对象
        Page<InspectRecordEntity> inspectRecord = baseService.pageRecord(inspectRecordEntity, pageModel);
        result.setCurrent(inspectRecord.getCurrent());
        result.setPages(inspectRecord.getPages());
        result.setSize(inspectRecord.getSize());
        result.setTotal(inspectRecord.getTotal());
        List<InspectRecordEntity> list = inspectRecord.getRecords();
        for(int i = 0,n = list.size();i < n;i++) {
            InspectRecordEntity record = inspectRecord.getRecords().get(i);
            record.setInspectdate(DaysUtil.dayBetween(record.getStartdate(),record.getEnddate()));
            list.set(i,record);
        }
        result.setRecords(list);
        // 返回业务分页数据
        return success(result);
    }

    @Log
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody InspectRecordCreateInputDTO inspectRecordCreateInputDTO) {
        InspectRecord inspectRecord = mInspectRecordMapper.toInspectRecord(inspectRecordCreateInputDTO);
        return super.save(inspectRecord);
    }

    @Log
    @ApiOperation(value = "修改")
    @PutMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody InspectRecordUpdateInputDTO inspectRecordUpdateInputDTO) {
        InspectRecord inspectRecord = mInspectRecordMapper.toInspectRecord(inspectRecordUpdateInputDTO);
        return super.update(inspectRecord);
    }

    @Log
    @ApiOperation(value = "巡检记录巡检项表分页查询")
    @GetMapping("/page")
    public R<PageResult<InspectRecordItemListOutputDTO>> pageList(InspectRecordItemSearchInputDTO inspectRecordItemSearchInputDTO, PageModel pageModel) {
        // 转换model
        InspectRecordItem inspectRecordItem = mInspectRecordItemMapper.toInspectRecordItem(inspectRecordItemSearchInputDTO);
        // 获取源对象
        IPage<InspectRecordItem> pageListByListInputDTO = inspectRecordItemService.pageListByEntity(inspectRecordItem, pageModel);
        // 转换新对象
        List<InspectRecordItemListOutputDTO> inspectRecordItemListOutputDTOS = mInspectRecordItemMapper.toOutputList(pageListByListInputDTO.getRecords());
        // 返回业务分页数据
        return success(toPageDTO(pageListByListInputDTO, inspectRecordItemListOutputDTOS));
    }


    static String desc = "已完成巡检{number}个；未完成巡检{endNumber}个；提交风险提示{riskNumber}个；提交状态报告{statusNumber}个；提交工作申请{jobNumber}个。";

    @Log
    @ApiOperation(value = "巡检审批情况查询")
    @GetMapping("/getItem")
    public R<ApproveRecordEntity> getItem(@NotBlank(message = "id不能为空") @RequestParam Long id) {
        ApproveRecordEntity dto = new ApproveRecordEntity();
        LambdaQueryWrapper<InspectRecordItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(InspectRecordItem::getRecordId,id);
        List<InspectRecordItem> recordItems = inspectRecordItemService.list(queryWrapper);
        InspectRecord byId = baseService.getById(id);
        ApproveRecord byId1 = approveRecordService.getById(byId.getId());
        if(byId1 != null){
            //dosomething
            dto.setUpdatedate(byId1.getUpdateDate().toString());
            dto.setUpdateby(byId1.getUpdateBy().toString());
            dto.setApprovestate(byId1.getApproveState());
            dto.setSpremark(byId1.getRemark());
        }
        int end_count = 0;
        int count  = recordItems.size();
        for(InspectRecordItem i : recordItems){
            if(StrUtil.isNotBlank(i.getInspectResult())){
                end_count++;
            }
        }
        //处理巡检情况描述字段
        dto.setSummarize(desc);
        dto.setRemark(byId.getRemark());
        return success(dto);
    }

    @Log
    @ApiOperation(value = "H5已处理页面")
    @GetMapping("/getPageList")
    public R<PageResult<InspectRecordEntity>> getPageList(InspectRecordEntity inspectRecordEntity, PageModel pageModel) throws ParseException {
        PageResult result =new PageResult();
        // 获取源对象
        Page<InspectRecordEntity> inspectRecord = baseService.pageRecord(inspectRecordEntity, pageModel);
        result.setCurrent(inspectRecord.getCurrent());
        result.setPages(inspectRecord.getPages());
        result.setSize(inspectRecord.getSize());
        result.setTotal(inspectRecord.getTotal());
        List<InspectRecordEntity> list = inspectRecord.getRecords();

        List<Long> recordIds = list.stream().map(InspectRecordEntity::getId).collect(Collectors.toList());

        LambdaQueryWrapper<InspectRecordItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.in(InspectRecordItem::getRecordId,recordIds);
        List<InspectRecordItem> recordItems = inspectRecordItemService.list(queryWrapper);

        for(int i = 0,n = list.size();i < n;i++) {
            InspectRecordEntity record = inspectRecord.getRecords().get(i);
            record.setInspectdate(DaysUtil.dayBetween(record.getStartdate(),record.getEnddate()));
            list.set(i,record);
            List<InspectRecordItem> collect = recordItems.stream().filter(map -> record.getId().equals(map.getRecordId())).collect(Collectors.toList());

        }
        result.setRecords(list);
        // 返回业务分页数据
        return success(result);
    }
}
