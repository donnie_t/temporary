package com.oeandn.modules.inspect.dtomapper;

import com.oeandn.modules.inspect.dto.input.InspectPathCreateInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPathSearchInputDTO;
import com.oeandn.modules.inspect.dto.input.InspectPathUpdateInputDTO;
import com.oeandn.modules.inspect.dto.output.InspectPathListOutputDTO;
import com.oeandn.system.model.InspectPath;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description: 巡检路线 转换类
 * @Date: 2022-08-09
 */
@Mapper(componentModel = "spring")
public interface MInspectPathMapper {

    InspectPath toInspectPath(InspectPathSearchInputDTO inspectPathSearchInputDTO);

    InspectPath toInspectPath(InspectPathCreateInputDTO inspectPathCreateInputDTO);

    InspectPath toInspectPath(InspectPathUpdateInputDTO inspectPathUpdateInputDTO);

    InspectPathListOutputDTO toOutput(InspectPath inspectPath);

    List<InspectPathListOutputDTO> toOutputList(List<InspectPath> inspectPaths);
}