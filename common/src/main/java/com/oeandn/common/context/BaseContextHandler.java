package com.oeandn.common.context;

import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：饮水机管理员
 * @Description: 线程帮助类
 * @Date: 2019/9/6 13:34
 */
public class BaseContextHandler {

    public static TransmittableThreadLocal<Map<String, Object>> threadLocal = new TransmittableThreadLocal<>();
    public static final String CONTEXT_KEY_USER_ID = "currentUserId";
    public static final String CONTEXT_KEY_DEPT_ID = "currentDeptId";
    public static final String CONTEXT_KEY_USERNAME = "currentUserName";

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        return map.get(key);
    }

    public static void setUserID(Long userID) {
        set(CONTEXT_KEY_USER_ID, userID);
    }

    public static Long getUserID() {
        Object value = get(CONTEXT_KEY_USER_ID);
        return returnObjectLongValue(value);
    }

    public static void setDeptID(Long userID) {
        set(CONTEXT_KEY_DEPT_ID, userID);
    }

    public static Long getDeptID() {
        Object value = get(CONTEXT_KEY_DEPT_ID);
        return returnObjectLongValue(value);
    }

    public static void setUsername(String username) {
        set(CONTEXT_KEY_USERNAME, username);
    }

    public static String getUsername() {
        Object value = get(CONTEXT_KEY_USERNAME);
        return returnObjectValue(value);
    }

    private static String returnObjectValue(Object value) {
        return value == null ? null : value.toString();
    }

    private static Long returnObjectLongValue(Object value) {
        return value == null ? null : Long.parseLong(value.toString());
    }

    public static void remove() {
        threadLocal.remove();
    }

}
