package com.oeandn.common.ws;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * 饮水机管理员
 */
@Service
@Slf4j
public class WsService {


    public synchronized void sendMsg(WebSocketSession session, String text) throws IOException {
        session.sendMessage(new TextMessage(text));
    }

  
    public synchronized void broadcastMsg(String text) throws IOException {
        for (WebSocketSession session: WebSocketSessionManager.sessionGroup.values()) {
            session.sendMessage(new TextMessage(text));
        }
    }

}
