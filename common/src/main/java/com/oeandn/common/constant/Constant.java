package com.oeandn.common.constant;

/**
 * @Author：饮水机管理员
 * @Description: 全局公共常量
 * @Date: 2019/9/6 13:36
 */
public interface Constant {

    String SESSION_USER_KEY = "UserInfo";
    /**
     * 存储用户权限
     */
    String REDIS_CAPTCHA_KEY = "user:captcha:";

    String COMMON_PARENT_ID = "0";
    String ROUTER_LAYOUT = "##";
    String ROUTER_PARENT_VIEW = "#";
    String ROUTER_NO_REDIRECT = "noRedirect";
}
