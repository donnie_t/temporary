package com.oeandn.common.message.local;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 消息封装bean
 * @author 饮水机管理员
 *
 */
@Data
public class EventBean implements Serializable {

	private static final long serialVersionUID = -5015970128307725471L;

	/**
	 * 消息类型
	 */
	private String eventType;

	/**
	 * 业务id
	 */
	private Long id;

	/**
	 * 内容替换map
	 */
	private Map<String, String> paramMap;
	
	/**
	 * 内容
	 */
	private String paramJson;



}
