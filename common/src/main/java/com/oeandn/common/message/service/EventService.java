package com.oeandn.common.message.service;

import java.util.Map;

/**
 * 事件服务接口
 * @author ll
 *
 */
public interface EventService {

	/**
	 * 触发事件
	 * @param eventType
	 * @param id
	 */
	public void event(String eventType, Long id);

	
	/**
	 * 触发事件
	 * @param eventType
	 * @param param  多个参数对的时候  用map封装
	 */
	public void event(String eventType, Map<String, String> param);
	
	/**
	 * 触发事件
	 * @param eventType
	 * @param paramJson  参数是个复杂对象的时候  转成json封装
	 */
	public void event(String eventType, String paramJson);
	
	/**
	 * 触发事件
	 * @param eventType
	 * @param id
	 * @param paramMap
	 * @param paramJson
	 */
	public void event(String eventType, Long id, Map<String, String> paramMap, String paramJson);
	
}
