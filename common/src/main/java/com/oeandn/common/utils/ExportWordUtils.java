package com.oeandn.common.utils;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ZipUtil;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.policy.ListRenderPolicy;
import com.deepoove.poi.util.PoitlIOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

@Slf4j
public class ExportWordUtils {


    public static final String fileDir = "/home/oebox/zip/File/";
    public static final String zip = "/home/oebox/zip/";

    public static final String root_zip = "/home/oebox/";

    public static final String formatSuffix=".docx";
    private static final String worker_exam = "template/worker_exam.docx";

    private static final String train_worker = "template/train_worker.docx";
    private static final String worker_paper = "template/worker_paper.docx";
    private static final String paper_info = "template/paper_info.docx";
    private static final String exam_info = "template/exam_info.docx";
    private static final String train_info = "template/train_info.docx";


    public static void download(String fileDir,String fileName,HttpServletRequest request,HttpServletResponse response) throws IOException {

        String tmpPath = fileDir + fileName;//新文件路径
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(fileName, CharsetUtil.UTF_8));
        response.setCharacterEncoding(CharsetUtil.UTF_8);
        FileInputStream file = new FileInputStream(tmpPath);
        IoUtil.copy(file, response.getOutputStream());
        delFile(fileDir, fileName);
    }

    public static void downloadZip(String src,String zip,String fileName,HttpServletRequest request,HttpServletResponse response) throws IOException {

        String zipPath = zip + fileName;
        ZipUtil.zip(src, zipPath);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(fileName, CharsetUtil.UTF_8));
        response.setCharacterEncoding(CharsetUtil.UTF_8);
        FileInputStream file = new FileInputStream(zipPath);
        IoUtil.copy(file, response.getOutputStream());
        removeZip(src,zip,fileName);
    }

    public static String zip(String src,String zip,String fileName){
        String zipPath = zip + fileName;
        ZipUtil.zip(src, zipPath);
        return zipPath;
    }

    public static void removeZip(String src,String zip,String fileName){
        delFile(zip,fileName);
        delFile(src,"");
    }

    public static void exportTrainWorker(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        ClassPathResource oldDoc = new ClassPathResource(train_worker);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;//新文件路径

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    public static void exportWord(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        ClassPathResource oldDoc = new ClassPathResource(worker_paper);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;//新文件路径

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    public static void exportPaperWord(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        ClassPathResource oldDoc = new ClassPathResource(paper_info);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    public static void exportExam(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        ClassPathResource oldDoc = new ClassPathResource(exam_info);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    public static void exportExamWorker(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }


        ClassPathResource oldDoc = new ClassPathResource(worker_exam);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    public static void exportTrainInfo(String fileDir,String fileName, Map<String, Object> params) throws Exception{
        File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        ClassPathResource oldDoc = new ClassPathResource(train_info);
        XWPFTemplate doc = XWPFTemplate.compile(oldDoc.getStream()).render(params);
        String tmpPath = fileDir + fileName;

        FileOutputStream fos = new FileOutputStream(tmpPath);
        doc.write(fos);
        fos.flush();
        fos.close();
        doc.close();
        PoitlIOUtils.closeQuietlyMulti(doc, fos);
    }

    private static void delFile(String filePath, String fileName){//删除文件
        File file = new File(filePath + fileName);
        File file1 = new File(filePath);
        if(file!=null){
            file.delete();
        }
        if(file1!=null){
            file1.delete();
        }
    }
}

