# oeandn-project-starter

## 平台简介

* 接口访问地址：[http://localhost:8768/doc.html](http://localhost:8768/doc.html)
    * 用户名：admin；密码：123456

## 技术介绍

|  技术   | 版本  |作用  |
|  ----  | ----  |----  |
| spring boot  | 2.5.4 |版本依赖，快速开发 |
| mybatis-plus  | 3.4.3.4 | 数据库持久层操作工具 |
| mybatis-plus-generator  | 3.4.1 | 代码生成器（controller、service、mapper、xml、entity、dto、dtomapper、vue） |
| sa-token  | 1.28.0 | java鉴权框架 |
| hutool  | 5.7.16 | java工具类 |
| flyway  | 7.7.3 | 数据库迁移工具 |
| knife4j  | 2.0.8 | 接口文档 |
| mapstruct  | 1.4.2.Final | DTO转换工具 |
| druid  | 1.1.21 | 数据库连接池 |
| redisson  | 3.12.5 | 分布式锁 |
| easypoi  | 4.4.0 | excel导出导入 |


## 内置功能

1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3. 岗位管理：配置系统用户所属担任职务。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7. 参数管理：对系统动态配置常用参数。
8. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
9. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
10. 代码生成：前后端代码的生成（controller、service、mapper、xml、entity、dto、dtomapper、vue）支持CRUD下载 。
11. 系统接口：根据业务代码自动生成相关的api接口文档。
12. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
13. 文件管理：fastDFS和Minio已经都集成。


## 数据权限怎么玩
1. 定义mapper 和 sql
```java
/**
 * 分页查询
 *
 * @param page    分页对象
 * @param wrapper 条件参数
 * @return
 */
@DataScope(value = {
        @DataColumn(alias = "userInfo", name = "dept_id")
})
IPage<UserInfo> selectPageList(IPage<UserInfo> page, @Param(Constants.WRAPPER) Wrapper wrapper);
```
```xml
<select id="selectPageList" resultType="com.oeandn.system.model.UserInfo">
    select <include refid="Base_Column_List_Alias"/>,
            dept.name as dept_name
    from sys_user_info userInfo left join sys_dept dept on userInfo.dept_id = dept.id
    ${ew.customSqlSegment}
</select>
```
2. service调用
```java
public IPage<UserInfo> pageList(UserInfo userInfo) {
    QueryWrapper<UserInfo> queryWrapper = Wrappers.<UserInfo>query();
    queryWrapper.eq("userInfo." + BaseModel.DEL_FLAG, BaseModelDelFlagEnum.NORMAL.code);
    queryWrapper.like(StrUtil.isNotBlank(userInfo.getUsername()), "userInfo." + UserInfo.USERNAME, userInfo.getUsername());
    if (StrUtil.isNotBlank(userInfo.getDeptId())) {
        queryWrapper.and(userInfoQueryWrapper -> {
                    userInfoQueryWrapper.eq("userInfo." + UserInfo.DEPT_ID, userInfo.getDeptId())
                            .or().inSql("userInfo." + UserInfo.DEPT_ID, "SELECT sys_dept.id FROM sys_dept WHERE FIND_IN_SET('" + userInfo.getDeptId() + "', ancestors)");
                }
        );
    }
    return baseMapper.selectPageList(getPagePlusInfo(userInfo), queryWrapper);
}
```
3. 关键性代码处理
```java
com.oeandn.db.base.interceptor.DataPermissionInterceptor#dataPermissionHandler()
```
4. 思路
    * 首先在需要的权限过滤的mapper上面加上`@DataScope`注解，然后添加`@DataColumn`字段，`alias`表示表别名；`name`表示字段名称
    * 然后 `com.oeandn.db.base.interceptor.DataPermissionInterceptor#dataPermissionHandler()` 会根据权限自动拼接sql 实现数据过滤
        * `@DataScope`注解 的 `type`字段可以为不同的类型单独处理不同的数据权限，`@DataColumn` 也是一样的逻辑，每个字段都可以单独处理
        * 查询数据权限 `com.oeandn.db.base.service.IDataScopeService` 接口

