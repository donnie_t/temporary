package com.oeandn.file.dtomapper;

import com.oeandn.file.dto.input.FileInfoSearchInputDTO;
import com.oeandn.file.dto.output.FileInfoListOutputDTO;
import com.oeandn.file.model.FileInfo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2020/11/12 11:20
 */
@Mapper(componentModel = "spring")
public interface MFileInfoMapper {

    FileInfo fileInfoSearchInputDTOToFileInfo(FileInfoSearchInputDTO fileInfoSearchInputDTO);

    FileInfoListOutputDTO fileInfoToFileInfoListOutputDTO(FileInfo fileInfo);

    List<FileInfoListOutputDTO> fileInfosToFileInfoListOutputDTOs(List<FileInfo> fileInfos);
}
