package com.oeandn.file.config;

import com.oeandn.file.properties.MinioProperties;
import com.oeandn.file.util.MinioUtil;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：饮水机管理员
 * @Description: minio配置类
 * @Date: 2021/4/20 14:13
 */
@Configuration
@ConditionalOnProperty(name = "file.type", havingValue = "minio")
public class MinioConfig {

    @Autowired
    private MinioProperties minioProperties;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder().endpoint(minioProperties.getUrl())
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey()).build();
    }

    @Bean
    public MinioUtil minioUtil(MinioProperties minioProperties, MinioClient minioClient) {
        return new MinioUtil(minioProperties, minioClient);
    }

}
