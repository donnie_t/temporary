package com.oeandn.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.file.model.FileInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2019-09-18
 */
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
