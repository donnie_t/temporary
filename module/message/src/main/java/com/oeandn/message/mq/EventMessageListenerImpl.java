package com.oeandn.message.mq;

import com.alibaba.fastjson.JSON;
import com.oeandn.common.message.local.EventBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * MQ消息处理类
 * 饮水机管理员
 */
@Slf4j
@Component
public class EventMessageListenerImpl {


	@Resource
	private EventProcess eventProcess;

	public void process(String message) {
		log.info("消费者收到消息开始消费:{}",message);
		try{
			EventBean eventBean=parseBody(message);
			eventProcess.process(eventBean);
		}catch (Exception e){
			log.error("消费异常：",e);
		}
	}

	private EventBean parseBody(String body){
		return JSON.parseObject(body, EventBean.class);
	}
}
