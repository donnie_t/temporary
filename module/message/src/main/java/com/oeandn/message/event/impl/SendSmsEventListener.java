package com.oeandn.message.event.impl;

import com.alibaba.fastjson.JSON;
import com.oeandn.common.message.annotation.MessageType;
import com.oeandn.common.message.local.EventBean;
import com.oeandn.message.event.AbsEventListener;
import com.oeandn.message.sms.SmsSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
@MessageType("发送短信")
public class SendSmsEventListener extends AbsEventListener {


	@Resource
	SmsSenderService smsSenderService;

	@Override
	public void onActionEvent(EventBean eventBean) {
		log.info("发送短信验证码",JSON.toJSONString(eventBean));
		try{
			if(eventBean == null){
				log.info("发送消息实体为空");
				return;
			}

			smsSenderService.send(eventBean);
		}catch (Exception e){
			log.error("新增系统日志:{}",e);
		}
	}
}

