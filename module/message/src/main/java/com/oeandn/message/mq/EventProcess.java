package com.oeandn.message.mq;

import com.alibaba.fastjson.JSON;
import com.oeandn.common.message.local.EventBean;
import com.oeandn.message.context.EventManager;
import com.oeandn.message.event.EventListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 消息分发类
 * 饮水机管理员
 */
@Slf4j
@Service("eventProcess")
public class EventProcess {



	@Resource
	private EventManager eventManager;

	public void process(EventBean eventBean) {
		if(eventBean != null){
			List<EventListener> listeners = eventManager.getListeners(eventBean.getEventType());
			if(listeners != null && listeners.size()>0){
				for(EventListener listener : listeners){
					try{
						listener.onActionEvent(eventBean);
					}catch(Exception e){
						log.error("事件处理异常", e);
						log.error( JSON.toJSONString(eventBean));
					}
				}
			}else{
				log.warn("没有对应的listener, EventType:" + eventBean.getEventType());
			}
					
		}else{
			log.error("eventBean为空");
		}
	}

}
