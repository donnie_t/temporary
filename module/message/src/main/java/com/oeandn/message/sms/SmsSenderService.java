package com.oeandn.message.sms;


import com.oeandn.common.message.local.EventBean;

/**
 * 短信发送
 * @author donnie
 *
 */
public interface SmsSenderService {

    /**
     * 发送短信验证码
     */
    void send(EventBean bean);

}
