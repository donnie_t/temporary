package com.oeandn.message.context;


import com.oeandn.common.message.annotation.MessageType;
import com.oeandn.message.event.EventListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 饮水机管理员
 */
@Slf4j
@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {


	@Resource
	private EventManager eventManager;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// TODO Auto-generated method stub
		log.info("程序上下文启动");
		Map<String, Object> beans = event.getApplicationContext().getBeansWithAnnotation(MessageType.class);
		for(Object bean :beans.values()){
			MessageType eventType = bean.getClass().getAnnotation(MessageType.class);
			eventManager.regEventListener(eventType.value(), (EventListener)bean);
		}

	}


}
