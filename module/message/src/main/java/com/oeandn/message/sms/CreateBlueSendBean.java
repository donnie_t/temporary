package com.oeandn.message.sms;

import java.io.Serializable;


public class CreateBlueSendBean implements Serializable {

	private static final long serialVersionUID = -5015970128307725471L;

	private String account;

	private String password;

	private String msg;

	private String phone;

	private String uid;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
