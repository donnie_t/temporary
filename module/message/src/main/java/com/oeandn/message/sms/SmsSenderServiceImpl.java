package com.oeandn.message.sms;

import com.alibaba.fastjson.JSON;
import com.oeandn.common.message.local.EventBean;
import com.oeandn.message.config.SmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class SmsSenderServiceImpl implements SmsSenderService {


	private static final String SMS_URL="http://smssh1.253.com/msg/send/json";

	@Resource
	SmsConfig smsConfig;

	@Override
	public void send(EventBean messageBean) {
		CreateBlueSendBean bean =new CreateBlueSendBean();
//		bean.setUid(messageBean.getMessageNo());
//		bean.setMsg(messageBean.getContext());
//		bean.setPhone(messageBean.getAccount());
		bean.setAccount(smsConfig.getAccount());
		bean.setPassword(smsConfig.getPassword());
		String sds =JSON.toJSONString(bean);
		//String res=HttpClientUtil.doHttpPost(SMS_URL, sds);
		//log.info("短信发送返回:{}",res);
	}
}
