package com.oeandn.message.service;

import java.util.Map;

import javax.annotation.Resource;

import com.oeandn.common.message.local.EventBean;
import com.oeandn.common.message.service.EventService;
import com.oeandn.message.mq.EventProcess;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService {

	@Resource
	EventProcess eventProcess;

	@Override
	public void event(String eventType, Long id) {
		// TODO Auto-generated method stub
		EventBean eventBean = new EventBean();
		eventBean.setEventType(eventType);
		eventBean.setId(id);
		eventProcess.process(eventBean);
	}

	@Override
	public void event(String eventType, Map<String, String> paramMap) {
		// TODO Auto-generated method stub
		EventBean eventBean = new EventBean();
		eventBean.setEventType(eventType);
		eventBean.setParamMap(paramMap);
		eventProcess.process(eventBean);
	}

	@Override
	public void event(String eventType, String paramJson) {
		// TODO Auto-generated method stub
		EventBean eventBean = new EventBean();
		eventBean.setEventType(eventType);
		eventBean.setParamJson(paramJson);
		eventProcess.process(eventBean);
	}

	@Override
	public void event(String eventType, Long id, Map<String, String> paramMap, String paramJson) {
		// TODO Auto-generated method stub
		EventBean eventBean = new EventBean();
		eventBean.setEventType(eventType);
		eventBean.setId(id);
		eventBean.setParamMap(paramMap);
		eventBean.setParamJson(paramJson);
		eventProcess.process(eventBean);
	}

}
