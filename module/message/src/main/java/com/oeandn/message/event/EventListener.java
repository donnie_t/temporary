package com.oeandn.message.event;


import com.oeandn.common.message.local.EventBean;

/**
 * 事件监听接口
 * @author 饮水机管理员
 *
 */
public interface EventListener {

	/**
	 * 处理事件
	 * @param eventBean  事件数据的封装
	 */
	public void onActionEvent(EventBean eventBean);

}
