package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.oeandn.common.model.TreeModel;
import com.oeandn.db.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 巡检点
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@Accessors(chain = true)
@TableName("inspect_point")
@EqualsAndHashCode(callSuper = true)
public class InspectPoint extends BaseModel implements TreeModel<InspectPoint>, Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 表主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 父巡检点id
     */
    private Long parentId;

    /**
     * 1.厂房 2房间 3设备
     */
    private String inspectPointType;

    /**
     * 巡检点名称，厂房名称，房间号，设备代码
     */
    private String inspectPointName;

    /**
     * 所属机组
     */
    private String subordinateUnits;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备负责人
     */
    private Long deviceTakeChargeBy;

    /**
     * 设备负责人
     */
    private String deviceTakeChargeName;

    /**
     * 设备标签id
     */
    private String deviceLebelId;

    /**
     * 设备标签名称
     */
    private String deviceLebelName;

    /**
     * 设备状态--字典  1正常 2异常 3 停产
     */
    private String deviceStatus;

    /**
     * 附件地址
     */
    private String accessory;

    /**
     * 坐标
     */
    private String coord;

    /**
     * 图标
     */
    private String icon;


    /**
     * 是否历史记录0否1是
     */
    private String historyCode;

    /**
     * 历史记录id
     */
    private Long historyId;


    public static final String ID = "id";

    public static final String PARENT_ID = "parent_id";

    public static final String INSPECT_POINT_TYPE = "inspect_point_type";

    public static final String INSPECT_POINT_NAME = "inspect_point_name";

    public static final String SUBORDINATE_UNITS = "subordinate_units";

    public static final String DEVICE_NAME = "device_name";

    public static final String DEVICE_TYPE = "device_type";

    public static final String DEVICE_TAKE_CHARGE_BY = "device_take_charge_by";

    public static final String DEVICE_TAKE_CHARGE_NAME = "device_take_charge_name";

    public static final String DEVICE_LEBEL_ID = "device_lebel_id";

    public static final String DEVICE_LEBEL_NAME = "device_lebel_name";

    public static final String DEVICE_STATUS = "device_status";

    public static final String ACCESSORY = "accessory";

    public static final String COORD = "coord";

    public static final String ICON = "icon";

    public static final String DEPT_ID = "dept_id";

    public static final String DEPT_ANCESTORS = "dept_ancestors";

    public static final String HISTORY_CODE = "history_code";

    public static final String HISTORY_ID = "history_id";
    @TableField(exist = false)
    private List<InspectPoint> children;
}
