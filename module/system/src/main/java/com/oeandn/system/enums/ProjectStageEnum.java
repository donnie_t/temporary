package com.oeandn.system.enums;

import java.util.stream.Stream;

/**
 * @Author：饮水机管理员
 * @Description: 项目阶段枚举
 * @Date: 2021/8/24 11:04
 */
public enum ProjectStageEnum {

    //资源 类型（1、目录；2、菜单；3、按钮）
    SALE("sale", "销售阶段"),
    CODE("code", "研发阶段"),
    SET("set", "交付阶段"),
    ;

    public final String code;
    public final String name;

    ProjectStageEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ProjectStageEnum getByCode(String code) {
        return Stream.of(ProjectStageEnum.values())
                .filter(resourcesTypeEnum -> resourcesTypeEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }
}
