package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.system.entity.InspectRecordEntity;
import com.oeandn.system.model.InspectPath;
import com.oeandn.system.model.InspectRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 巡检记录表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
public interface InspectRecordMapper extends BaseMapper<InspectRecord> {

    @Select("SELECT " +
            "re.id" +
            "pa.path_name, " +
            "pa.path_code, " +
            "pa.version, " +
            "re.create_date, " +
            "re.update_by, " +
            "pa.dept_id, " +
            "pa.inspect_on_duty, " +
            "re.update_date, " +
            "re.data_complete,  " +
            "re.start_date, " +
            "re.end_date,  " +
            "re.submit_date" +
            "sde.`name`" +
            "FROM " +
            "inspect_record AS re " +
            "INNER JOIN inspect_path AS pa ON re.path_id = pa.id  " +
            "LEFT JOIN sys_dept as sde on re.dept_id = sde.id" +
            " ${ew.customSqlSegment} ")
    Page<InspectRecordEntity> queryInspectPage(IPage page, @Param("ew") Wrapper wrapper);
    //review userController   ViewMapper

//     +
//             "1 = 1  " +
//             "AND pa.path_name = ''  " +
//             "AND pa.path_code = ''  " +
//             "AND re.create_date >= ''  " +
//             "AND re.create_date <= ''  " +
//             "AND re.update_by = ''  " +
//             "AND pa.inspect_on_duty = ''  " +
//             "AND data_complete = ''"

}
