package com.oeandn.system.entity;

import com.oeandn.common.model.PageModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 包名：com.oeandn.system.entity<br>
 * 文件名：PointApproveSelectDTO<br>
 * 版权：Copyright by xxxx公司<br>
 * 描述：<br>
 * 修改人：GGW<br>
 * 修改时间：2022-08-12 16:29<br>
 * 跟踪单号：<br>
 * 修改单号：<br>
 * 修改内容：<br>
 **/
@Data
@Accessors(chain = true)
@ApiModel(value = "PointApproveSelectDTO", description = "巡检点查询条件")
public class PointApproveSelectDTO extends PageModel {


    @ApiModelProperty(value = "巡检点")
    private String inspectPointName;


    @ApiModelProperty(value = "审批状态 1.已审批 2 待审批")
    private int approvalStatus = 0;



}