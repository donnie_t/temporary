package com.oeandn.system.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: lwl
 * @Date: 2022/08/15/11:19
 * @Description:
 */
@Data
@Accessors(chain = true)
public class InspectResultRecordEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "业务主键id")
    private String businessId;

    @ApiModelProperty(value = "线路类型")
    private String pathType;

    @ApiModelProperty(value = "路线名称")
    private String pathName;

    @ApiModelProperty(value = "路线编号")
    private String pathCode;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "任务创建时间")
    private String taskCreateDate;

    @ApiModelProperty(value = "业务类型1巡检路线2巡检点3巡检记录4巡检项目")
    private String businessType;

    @ApiModelProperty(value = "审批人id")
    private String approveUserId;

    @ApiModelProperty(value = "审批人名称")
    private String userName;

    @ApiModelProperty(value = "审批状态（0-待审批，1-审批通过，2-审批拒绝）")
    private String approveState;

    @ApiModelProperty(value = "创建者(巡检人id)")
    private String createBy;

    @ApiModelProperty(value = "创建者(巡检人)")
    private String createName;

    @ApiModelProperty(value = "创建时间(任务提交时间)")
    private String createDate;

    @ApiModelProperty(value = "更新者")
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    private String updateDate;

    @ApiModelProperty(value = "巡检开始时间")
    private String startdate;

    @ApiModelProperty(value = "巡检结束时间")
    private String enddate;

    @ApiModelProperty(value = "巡检用时")
    private String inspectdate;

    @ApiModelProperty(value = "数据标识-0正常1删除2待审核")
    private String delFlag;

    @ApiModelProperty(value = "备注")
    private String remark;

    public static final String ID = "id";

    public static final String DEPT_ID = "dept_id";

    public static final String BUSINESS_ID = "business_id";

    public static final String PATH_TYPE = "path_type";

    public static final String PATH_NAME = "path_name";

    public static final String PATH_CODE = "path_code";

    public static final String VERSION = "version";

    public static final String TASK_CREATE_DATE = "task_create_date";

    public static final String BUSINESS_TYPE = "business_type";

    public static final String APPROVEUSER_ID = "approveuser_id";

    public static final String USERNAME = "username";

    public static final String APPROVE_STATE = "approve_state";

    public static final String CREATE_BY = "create_by";

    public static final String CREATE_NAME = "create_name";

    public static final String CREATE_DATE = "create_date";

    public static final String UPDATE_DATE = "update_date";

    public static final String UPDATE_BY = "update_by";

    public static final String START_DATE = "start_date";

    public static final String END_DATE = "end_date";

    public static final String INSPECT_DATE = "inspect_date";

    public static final String DEL_FLAG = "del_flag";

    public static final String REMARK = "remark";

}
