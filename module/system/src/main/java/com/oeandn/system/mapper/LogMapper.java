package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.Log;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-01-06
 */
public interface LogMapper extends BaseMapper<Log> {

}
