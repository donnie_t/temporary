package com.oeandn.system.mapper;

import com.oeandn.system.model.ScheduleInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 排班 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
public interface ScheduleInfoMapper extends BaseMapper<ScheduleInfo> {

}
