package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.oeandn.db.base.model.BaseModel;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * 排班
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Data
@Accessors(chain = true)
public class ScheduleInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 排班日期
     */
    private Date scheduleDate;

    /**
     * 星期几1到7
     */
    private Integer weekCode;

    /**
     * 早班，1值,2值,3值，多个逗号隔开
     */
    private String morningShift;

    /**
     * 中班，1值,2值,3值，多个逗号隔开
     */
    private String noonShift;

    /**
     * 晚班，1值,2值,3值，多个逗号隔开
     */
    private String eveningShift;

    @TableField(exist = false)
    private String yearMonth;


    public static final String ID = "id";

    public static final String SCHEDULE_DATE = "schedule_date";

    public static final String WEEK_CODE = "week_code";

    public static final String MORNING_SHIFT = "morning_shift";

    public static final String NOON_SHIFT = "noon_shift";

    public static final String EVENING_SHIFT = "evening_shift";

}
