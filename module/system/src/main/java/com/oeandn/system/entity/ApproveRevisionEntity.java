package com.oeandn.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: lwl
 * @Date: 2022/08/17/17:07
 * @Description:
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ApproveRevisionEntity", description = "审批修订")
public class ApproveRevisionEntity implements Serializable{

    @ApiModelProperty(value = "巡检路线id")
    private String history_id;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "更新者id")
    private Long updateBy;

    @ApiModelProperty(value = "更新者姓名")
    private String updateName;

    @ApiModelProperty(value = "审批人id")
    private Long approveUserId;

    @ApiModelProperty(value = "审批人姓名")
    private String approveUserName;

    @ApiModelProperty(value = "修订时间")
    private String pathUpdateDate;

    @ApiModelProperty(value = "审批时间")
    private String approveUpdateDate;


    public static final String HISTORY_ID = "history_id";

    public static final String VERSION = "version";

    public static final String REMARK = "remark";

    public static final String UPDATE_BY = "update_by";

    public static final String UPDATE_NAME = "update_name";

    public static final String APPROVE_USER_ID = "approve_user_id";

    public static final String APPROVE_USER_NAME = "approve_user_name";

    public static final String PATH_UPDATE_DATE = "path_update_date";

    public static final String APPROVE_UPDATE_DATE = "approve_update_date";
}
