package com.oeandn.system.model;

import com.oeandn.db.base.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 巡检记录表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class InspectRecord extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 路线id
     */
    private Long pathId;

    /**
     * 0空缺1完整2部分完整
     */
    private String dataComplete;

    /**
     * 审批id
     */
    private Long approveId;

    /**
     * 任务创建时间
     */
    private Date taskCreateDate;

    /**
     * 巡检开始时间
     */
    private Date startDate;

    /**
     * 巡检结束时间
     */
    private Date endDate;

    /**
     * 任务提交时间
     */
    private Date submitDate;

    /**
     * 巡检状态1未开始巡检2巡检进行中3巡检已完成
     */
    private String status;


    public static final String ID = "id";

    public static final String PATH_ID = "path_id";

    public static final String DATA_COMPLETE = "data_complete";

    public static final String APPROVE_ID = "approve_id";

    public static final String TASK_CREATE_DATE = "task_create_date";

    public static final String START_DATE = "start_date";

    public static final String END_DATE = "end_date";

    public static final String SUBMIT_DATE = "submit_date";

    public static final String STATUS = "status";
}
