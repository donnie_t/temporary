package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.system.model.DangerZone;
import com.oeandn.system.mapper.DangerZoneMapper;
import com.oeandn.db.base.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 危险区-红橙区 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Service
public class DangerZoneService extends BaseService<DangerZoneMapper, DangerZone> {

    /**
     * 获取表格数据
     *
     * @param dangerZone
     * @param pageModel
     * @return
     */
    public IPage<DangerZone> pageListByEntity(DangerZone dangerZone, PageModel pageModel) {
        LambdaQueryWrapper<DangerZone> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(dangerZone.getZoneName()), DangerZone::getZoneName, dangerZone.getZoneName());
        queryWrapper.like(StrUtil.isNotBlank(dangerZone.getRoomNo()), DangerZone::getRoomNo, dangerZone.getRoomNo());
        queryWrapper.eq(StrUtil.isNotBlank(dangerZone.getZoneTag()), DangerZone::getZoneTag, dangerZone.getZoneTag());
        queryWrapper.eq(StrUtil.isNotBlank(dangerZone.getDelFlag()), DangerZone::getDelFlag, dangerZone.getDelFlag());
        return super.page(pageModel, queryWrapper);
    }

}
