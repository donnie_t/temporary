package com.oeandn.system.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 巡检路线
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@Accessors(chain = true)
public class ApproveRecordEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "巡检情况描述")
    private String summarize;

    @ApiModelProperty(value = "巡检备注")
    private String remark;

    @ApiModelProperty(value = "审批时间")
    private String updatedate;

    @ApiModelProperty(value = "审批人")
    private String updateby;

    @ApiModelProperty(value = "审批结果")
    private String approvestate;

    @ApiModelProperty(value = "任务提交时间")
    private String submitDate;

    @ApiModelProperty(value = "审批备注")
    private String spremark;



    public static final String ID = "id";

    public static final String SUMMARIZE = "summarize";

    public static final String REMARK = "remark";

    public static final String UPDATE_DATE = "update_date";

    public static final String UPDATE_BY = "update_by";

    public static final String APPROVE_STATE = "approve_state";

    public static final String SUBMIT_DATE = "submit_date";

    public static final String SPREMARK = "spremark";

}
