package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.system.entity.ApproveRevisionEntity;
import com.oeandn.system.entity.InspectPointApproveListDTO;
import com.oeandn.system.entity.InspectResultRecordEntity;
import com.oeandn.system.model.ApproveRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 巡检审批表 Mapper 接口 15197681435
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
public interface ApproveRecordMapper extends BaseMapper<ApproveRecord> {

    @Select("SELECT " +
            "a.id" +
            "c.path_name, " +
            "c.path_code, " +
            "c.version, " +
            "b.create_by, " +
            "e.username as createname, " +
            "b.task_create_date, " +
            "b.create_date, " +
            "b.start_date, " +
            "b.end_date, " +
            "b.data_complete, " +
            "a.approve_user_id, " +
            "d.username, " +
            "a.approve_state " +
            "FROM " +
            "approve_record AS a " +
            "LEFT JOIN inspect_record as b ON a.business_id = b.id " +
            "LEFT JOIN inspect_path as c ON c.id = a.business_id " +
            "LEFT JOIN sys_user_info as d ON a.approve_user_id = d.id " +
            "LEFT JOIN sys_user_info as e ON b.create_by = e.id " +
            "WHERE a.business_type = '3' " +
            " ${ew.customSqlSegment} ")
    Page<InspectResultRecordEntity> queryApprovePage(IPage page, @Param("ew") Wrapper wrapper);

    @Select("  SELECT  " +
            "  ip.*,  " +
            "  a.inspect_point_id,  " +
            "  a.approvalPending,  " +
            "  a.approve,  " +
            "  a.approveRefused ,  " +
            "  a.approve_state  " +
            "FROM  " +
            "  inspect_point AS ip  " +
            "  LEFT JOIN (SELECT  " +
            "  pi.inspect_point_id   " +
            "  ,SUM(if(ar.approve_state ='0',1,0))approvalPending  " +
            "  ,SUM(if(ar.approve_state ='1',1,0))approve  " +
            "   ,SUM(if(ar.approve_state ='2',1,0))approveRefused  " +
            "  ,if((SUM(if(ar.approve_state ='0',1,0))) > 0,1,0)approve_state    " + //1是待审批  0审批完成
            "FROM  " +
            "  inspect_point_item AS pi  " +
            "  LEFT JOIN  " +
            "  approve_record AS ar  " +
            "  ON   " +
            "    pi.id = ar.business_id AND  " +
            "    ar.business_type = 4  " +
            "GROUP BY  " +
            "  pi.inspect_point_id)a ON ip.id = a.inspect_point_id  " +

            "   ${ew.customSqlSegment} ")
    Page<InspectPointApproveListDTO> pointPage(Page page, @Param("ew") QueryWrapper<ApproveRecord> wrapper);

    @Select("SELECT " +
            "b.version, " +
            "b.remark, " +
            "b.update_by, " +
            "a.approve_user_id, " +
            "b.update_date " +
            "FROM " +
            "approve_record AS a " +
            "LEFT JOIN inspect_path AS b ON b.id = a.business_id " +
            "WHERE " +
            "a.business_type = '1' " +
            "and b.history_code = '1' " +
            " ${ew.customSqlSegment} ")
    Page<ApproveRevisionEntity> approveReisionPage(IPage page, @Param("ew") Wrapper wrapper);
}
