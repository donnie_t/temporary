package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.oeandn.db.base.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * 危险区-红橙区
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DangerZone extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 红橙区名称
     */
    private String zoneName;

    /**
     * 房间号
     */
    private String roomNo;

    /**
     * 坐标1
     */
    private String coordOne;

    /**
     * 坐标2
     */
    private String coordTwo;

    /**
     * 坐标3
     */
    private String coordThree;

    /**
     * 坐标4
     */
    private String coordFour;

    /**
     * 1橙区2红区
     */
    private String zoneTag;




    public static final String ID = "id";

    public static final String ZONE_NAME = "zone_name";

    public static final String ROOM_NO = "room_no";

    public static final String COORD_ONE = "coord_one";

    public static final String COORD_TWO = "coord_two";

    public static final String COORD_THREE = "coord_three";

    public static final String COORD_FOUR = "coord_four";

    public static final String ZONE_TAG = "zone_tag";


}
