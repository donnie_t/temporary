package com.oeandn.system.mapper;

import com.oeandn.system.model.InspectPointItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检项 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
public interface InspectPointItemMapper extends BaseMapper<InspectPointItem> {

}
