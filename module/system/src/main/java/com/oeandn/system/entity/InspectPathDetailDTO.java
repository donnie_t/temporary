package com.oeandn.system.entity;
/**
 * @Auther: GGW
 * @Date: 2022/8/11 15:00
 * @Description:
 */

import com.oeandn.system.model.InspectPath;
import com.oeandn.system.model.InspectPoint;
import com.oeandn.system.model.InspectPointItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 包名：com.oeandn.system.entity<br>
 * 文件名：InspectPathDetailDTO.java<br>
 * 版权：Copyright by xxxx公司<br>
 * 描述：<br>
 * 修改人：GGW<br>
 * 修改时间：2022-08-11 15:00<br>
 * 跟踪单号：<br>
 * 修改单号：<br>
 * 修改内容：<br>
 **/
@Data
@Accessors(chain = true)
@ApiModel(value="InspectPathDetailDTO", description="路线详情")
public class InspectPathDetailDTO {
    @ApiModelProperty(value = "路线详情")
    private InspectPath path;

    @ApiModelProperty(value = "路线详情")
    private List<InspectPath> pathHistory;

    @ApiModelProperty(value = "巡检项")
    private List<InspectPointItem> itemList;

    @ApiModelProperty(value = "巡检点用于地图")
    private List<InspectPoint> points;

}