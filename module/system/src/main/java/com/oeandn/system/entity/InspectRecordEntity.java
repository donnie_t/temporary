package com.oeandn.system.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 巡检路线
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@Accessors(chain = true)
public class InspectRecordEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "线路类型")
    private String pathType;

    @ApiModelProperty(value = "巡检路线名称")
    private String pathName;

    @ApiModelProperty(value = "巡检路线编号")
    private String pathCode;

    @ApiModelProperty(value = "巡检周期")
    private String inspectCycle;

    @ApiModelProperty(value = "巡检人")
    private String createby;

    @ApiModelProperty(value = "巡检班值")
    private String inspectOnDuty;

    @ApiModelProperty(value = "完整性")
    private String datacomplete;

    @ApiModelProperty(value = "部门名称")
    private String name;

    @ApiModelProperty(value = "次序")
    private String sequence;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "概括")
    private String summarize;

    @ApiModelProperty(value = "任务提交时间")
    private String submitDate;

    @ApiModelProperty(value = "巡检开始时间")
    private String startdate;

    @ApiModelProperty(value = "巡检结束时间")
    private String enddate;

    @ApiModelProperty(value = "巡检用时")
    private String inspectdate;

    @ApiModelProperty(value = "是否历史记录")
    private String historyCode;

    @ApiModelProperty(value = "历史记录id")
    private Long historyId;


    public static final String ID = "id";

    public static final String PATH_TYPE = "path_type";

    public static final String PATH_NAME = "path_name";

    public static final String PATH_CODE = "path_code";

    public static final String INSPECT_CYCLE = "inspect_cycle";

    public static final String CREATE_BY = "create_by";

    public static final String INSPECT_ON_DUTY = "inspect_on_duty";

    public static final String DATA_COMPLETE = "data_complete";

    public static final String NAME = "name";

    public static final String SEQUENCE = "sequence";

    public static final String VERSION = "version";

    public static final String SUMMARIZE = "summarize";

    public static final String SUBMIT_DATE = "submit_date";

    public static final String START_DATE = "start_date";

    public static final String END_DATE = "end_date";

    public static final String INSPECT_DATE = "inspect_date";

    public static final String HISTORY_CODE = "history_code";

    public static final String HISTORY_ID = "history_id";

}
