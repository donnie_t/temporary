package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.db.base.service.BaseService;
import com.oeandn.system.mapper.DictDataMapper;
import com.oeandn.system.mapper.DictTypeMapper;
import com.oeandn.system.model.DictData;
import com.oeandn.system.model.DictType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Service
public class DictTypeService extends BaseService<DictTypeMapper, DictType> {

    @Resource
    private DictDataMapper dictDataMapper;

    /**
     * 分页查询
     *
     * @param dictType
     * @return
     */
    public IPage<DictType> pageListByEntity(DictType dictType, PageModel pageModel) {
        LambdaQueryWrapper<DictType> queryWrapper = Wrappers.<DictType>lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(dictType.getDictName()), DictType::getDictName, dictType.getDictName());
        queryWrapper.eq(StrUtil.isNotBlank(dictType.getDictStatus()), DictType::getDictStatus, dictType.getDictStatus());
        return super.page(pageModel, queryWrapper);
    }

    /**
     * 删除类型 和 删除数据
     *
     * @param id
     * @return
     */
    public boolean deleteDictTypeById(Long id) {
        final DictType dictType = getById(id);
        if (dictType != null) {
            dictDataMapper.delete(Wrappers.<DictData>lambdaQuery().eq(DictData::getParentId, dictType.getId()));
        }
        return removeById(id);
    }
}
