package com.oeandn.system.mapper;

import com.oeandn.system.model.InspectPathPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 路线巡检点关联表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
public interface InspectPathPointMapper extends BaseMapper<InspectPathPoint> {

}
