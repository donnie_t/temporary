package com.oeandn.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * /**
 * 包名：com.oeandn.system.entity<br>
 * 文件名：InspectPointApproveListDTO<br>
 * 版权：Copyright by xxxx公司<br>
 * 描述：<br>
 * 修改人：GGW<br>
 * 修改时间：2022-08-11 15:57<br>
 * 跟踪单号：<br>
 * 修改单号：<br>
 * 修改内容：<br>
 **/
@Data
@Accessors(chain = true)
@ApiModel(value = "InspectPointApproveListDTO", description = "巡检点审批列表展示")
public class InspectPointApproveListDTO {

    //机组
    @ApiModelProperty(value = "机组")
    private String unit;

    //巡检点
    @ApiModelProperty(value = "巡检点")
    private String inspectPointName;

    //待审批数量
    @ApiModelProperty(value = "待审批数量")
    private int approvalPending;

    //审批通过数量
    @ApiModelProperty(value = "审批通过数量")
    private int approve;

    //审批拒绝数量
    @ApiModelProperty(value = "审批拒绝数量")
    private int approveRefused;

    //审批状态 1.已审批 2 待审批
    @ApiModelProperty(value = "审批状态 1.已审批 2 待审批")
    private int approvalStatus;

    //提交时间
    @ApiModelProperty(value = "提交时间")
    private Date createDate;

    //审批时间
    @ApiModelProperty(value = "审批时间")
    private Date updateDate;


}