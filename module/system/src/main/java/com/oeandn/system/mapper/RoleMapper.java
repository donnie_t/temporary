package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2020-08-19
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据角色id 获取能看到的部门ids
     *
     * @param roleId 角色id
     * @return
     */
    Set<Long> selectAuthDeptIdsByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据用户id 查询角色标识
     *
     * @param userId 用户id
     * @return
     */
    List<Long> selectRoleKeyListByUserId(@Param("userId") Long userId);

    /**
     * 根据用户id 查询角色列表
     *
     * @param userId
     * @return
     */
    List<Role> selectRoleListByUserId(@Param("userId") Long userId);
}
