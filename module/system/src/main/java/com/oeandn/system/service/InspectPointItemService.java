package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.db.base.service.BaseService;
import com.oeandn.system.entity.InspectPointItemDetailDTO;
import com.oeandn.system.mapper.InspectPointItemMapper;
import com.oeandn.system.model.InspectPoint;
import com.oeandn.system.model.InspectPointItem;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 巡检项 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Service
public class InspectPointItemService extends BaseService<InspectPointItemMapper, InspectPointItem> {
    @Resource
    InspectPathService inspectPathService;

    @Resource
    InspectPointService inspectPointService;
    @Resource
    ApproveRecordService approveRecordService;

    /**
     * 获取表格数据
     *
     * @param inspectPointItem
     * @param pageModel
     * @return
     */
    public IPage<InspectPointItem> pageListByEntity(InspectPointItem inspectPointItem, PageModel pageModel) {
        LambdaQueryWrapper<InspectPointItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getInspectPlace()), InspectPointItem::getInspectPlace,
                inspectPointItem.getInspectPlace());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getDeviceCode()), InspectPointItem::getDeviceCode,
                inspectPointItem.getDeviceCode());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getItemName()), InspectPointItem::getItemName,
                inspectPointItem.getItemName());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getPropertyCode()), InspectPointItem::getPropertyCode,
                inspectPointItem.getPropertyCode());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getPropertyName()), InspectPointItem::getPropertyName,
                inspectPointItem.getPropertyName());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getUpperLimit()), InspectPointItem::getUpperLimit,
                inspectPointItem.getUpperLimit());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getLowerLimit()), InspectPointItem::getLowerLimit,
                inspectPointItem.getLowerLimit());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getHighAlert()), InspectPointItem::getHighAlert,
                inspectPointItem.getHighAlert());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getLowAlert()), InspectPointItem::getLowAlert,
                inspectPointItem.getLowAlert());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getTypicalValue()), InspectPointItem::getTypicalValue,
                inspectPointItem.getTypicalValue());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getUnitName()), InspectPointItem::getUnitName,
                inspectPointItem.getUnitName());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getAbnormalDeviationValue()),
                InspectPointItem::getAbnormalDeviationValue, inspectPointItem.getAbnormalDeviationValue());
        queryWrapper.like(StrUtil.isNotBlank(inspectPointItem.getHistoryCode()), InspectPointItem::getHistoryCode,
                inspectPointItem.getHistoryCode());


        return super.page(pageModel, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean saveInspectPointItem(InspectPointItem inspectPointItem) {
        inspectPointItem.setDelFlag("2");
        boolean save = this.save(inspectPointItem);
        //判断是否需要审批
        if (save) {
            //需要审批
            Long id = inspectPointItem.getId();
            // 调用审批
            return approveRecordService.saveApproveRecord(inspectPointItem.getDeptId(),id,"4","0");
        }

        return false;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean updateInspectPointItem(InspectPointItem inspectPointItem) {

        InspectPointItem item = new InspectPointItem();

        item.setInspectPointId(inspectPointItem.getInspectPointId());
        item.setInspectPlace(inspectPointItem.getInspectPlace());
        item.setDeviceCode(inspectPointItem.getDeviceCode());
        item.setItemName(inspectPointItem.getUnitName());
        item.setPropertyCode(inspectPointItem.getPropertyCode());
        item.setPropertyName(inspectPointItem.getPropertyName());
        item.setUpperLimit(inspectPointItem.getUpperLimit());
        item.setLowerLimit(inspectPointItem.getLowerLimit());
        item.setHighAlert(inspectPointItem.getHighAlert());
        item.setLowAlert(inspectPointItem.getLowAlert());
        item.setTypicalValue(inspectPointItem.getTypicalValue());
        item.setUnitName(inspectPointItem.getUnitName());
        item.setAbnormalDeviationValue(inspectPointItem.getAbnormalDeviationValue());

        item.setHistoryId(inspectPointItem.getHistoryId());
        item.setDelFlag("2");
        boolean save = this.save(item);
        //判断是否需要审批

        if (save) {
            //需要审批
            Long id = item.getId();
            //TODO 调用审批
            return approveRecordService.saveApproveRecord(inspectPointItem.getDeptId(),id,"4","0");
        }

        return false;
    }

    /**
     * 功能描述: 巡检项审批通过调用
     *
     * @param id 审批表业务主键id
     * @return {@link null}
     * @author GGW
     * @date 2022-08-10
     */
    @Transactional(rollbackFor = Exception.class)
    public void upd(Long id) {
        InspectPointItem pointItem = this.getById(id);

        //修改旧数据为历史数据
        InspectPointItem oldItem = this.getById(pointItem.getHistoryId());
        oldItem.setHistoryId(id);
        oldItem.setHistoryCode("1");
        this.updateById(oldItem);

        //修改路线关联巡检项表
        inspectPathService.updateInspectPathPoint(pointItem.getHistoryId(), id);

        //修改主数据状态
        pointItem.setHistoryId(0L);
        pointItem.setDelFlag("0");
        this.updateById(pointItem);
    }

    public InspectPointItemDetailDTO getdetail(Long id) {
        InspectPointItemDetailDTO dto = new InspectPointItemDetailDTO();

        InspectPointItem item = getById(id);
        if(!StringUtils.isEmpty(item)){
            dto.setItem(item);

            List<InspectPointItem> list = list(new QueryWrapper<InspectPointItem>().eq(InspectPointItem.HISTORY_ID, id));
            dto.setItems(list);

            InspectPoint point = inspectPointService.getById(item.getInspectPointId());
            if(StringUtils.isEmpty(point)){
                dto.setPoint(new InspectPoint());
            }
            dto.setPoint(point);
        }
        return dto;
    }
}
