package com.oeandn.system.mapper;

import com.oeandn.system.model.IdeaFeedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 意见反馈 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
public interface IdeaFeedbackMapper extends BaseMapper<IdeaFeedback> {

}
