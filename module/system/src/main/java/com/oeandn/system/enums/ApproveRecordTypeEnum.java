package com.oeandn.system.enums;

import java.util.stream.Stream;

/**
 * @Author：饮水机管理员
 * @Description: 用户状态枚举类
 * @Date: 2021/8/24 11:08
 */
public enum ApproveRecordTypeEnum {

    //1巡检路线2巡检点3巡检记录4巡检项目
    PATH("1", "巡检路线"),
    POINT("2", "巡检点"),
    RECORD("3", "巡检记录"),
    PROJECT("4", "巡检项目"),
    ;

    public final String code;
    public final String name;

    ApproveRecordTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ApproveRecordTypeEnum getByCode(String code) {
        return Stream.of(ApproveRecordTypeEnum.values())
                .filter(userInfoStatusEnum -> userInfoStatusEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }

}
