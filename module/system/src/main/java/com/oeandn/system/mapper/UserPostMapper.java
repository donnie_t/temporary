package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.UserPost;

/**
 * <p>
 * 用户与岗位关联表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
public interface UserPostMapper extends BaseMapper<UserPost> {

}
