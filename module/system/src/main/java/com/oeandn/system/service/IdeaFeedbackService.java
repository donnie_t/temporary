package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.system.model.IdeaFeedback;
import com.oeandn.system.mapper.IdeaFeedbackMapper;
import com.oeandn.db.base.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 意见反馈 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Service
public class IdeaFeedbackService extends BaseService<IdeaFeedbackMapper, IdeaFeedback> {

    /**
     * 获取表格数据
     *
     * @param ideaFeedback
     * @param pageModel
     * @return
     */
    public IPage<IdeaFeedback> pageListByEntity(IdeaFeedback ideaFeedback, PageModel pageModel) {
        LambdaQueryWrapper<IdeaFeedback> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(StrUtil.isNotBlank(ideaFeedback.getFeedbackType()), IdeaFeedback::getFeedbackType, ideaFeedback.getFeedbackType());
        queryWrapper.eq(StrUtil.isNotBlank(ideaFeedback.getDelFlag()), IdeaFeedback::getDelFlag, ideaFeedback.getDelFlag());
        if(ideaFeedback.getStartDate() !=null && ideaFeedback.getEndDate() != null ){
            queryWrapper.ge(IdeaFeedback::getCreateDate,ideaFeedback.getStartDate());
            queryWrapper.le(IdeaFeedback::getCreateDate,ideaFeedback.getEndDate());
        }
        return super.page(pageModel, queryWrapper);
    }

}
