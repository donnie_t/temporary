package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.oeandn.db.base.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 巡检路线
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class InspectPath extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 表主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 路线类型-字典    1.日常巡检 2.大修巡检
     */
    private String pathType;

    /**
     * 巡检路线名称
     */
    private String pathName;

    /**
     * 巡检路线编号
     */
    private String pathCode;

    /**
     * 巡检周期
     */
    private String inspectCycle;

    /**
     * 巡检班值
     */
    private String inspectOnDuty;

    /**
     * 次序
     */
    private String sequence;

    /**
     * 版本号
     */
    private String version;

    /**
     * 概括
     */
    private String summarize;


    /**
     * 是否历史记录0否1是
     */
    private String historyCode;

    /**
     * 历史记录id
     */
    private Long historyId;

    /**
     * 路线状态    默认0.启用 1.停用
     */
    private String pathStatus;


    public static final String ID = "id";

    public static final String PATH_TYPE = "path_type";

    public static final String PATH_NAME = "path_name";

    public static final String PATH_CODE = "path_code";

    public static final String INSPECT_CYCLE = "inspect_cycle";

    public static final String INSPECT_ON_DUTY = "inspect_on_duty";

    public static final String SEQUENCE = "sequence";

    public static final String VERSION = "version";

    public static final String SUMMARIZE = "summarize";

    public static final String INSPECT_POINT_ID = "inspect_point_id";

    public static final String HISTORY_CODE = "history_code";

    public static final String HISTORY_ID = "history_id";

    public static final String PATH_STATUS = "path_status";

    @ApiModelProperty(value = "巡检项ids")
    @TableField(exist = false)
    private List<Long> ids;

    /**
     * 审批状态（0-待审批，1-审批通过，2-审批拒绝）
     */
    @ApiModelProperty(value = "审批状态（0-待审批，1-审批通过，2-审批拒绝）")
    @TableField(exist = false)
    private String approveState;
}
