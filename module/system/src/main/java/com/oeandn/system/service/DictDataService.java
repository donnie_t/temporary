package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.db.base.service.BaseService;
import com.oeandn.system.enums.DictDataStatusEnum;
import com.oeandn.system.mapper.DictDataMapper;
import com.oeandn.system.model.DictData;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
@Service
public class DictDataService extends BaseService<DictDataMapper, DictData> {

    /**
     * 获取表格数据
     *
     * @param dictData
     * @return
     */
    public IPage<DictData> pageListByEntity(DictData dictData, PageModel pageModel) {
        LambdaQueryWrapper<DictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(dictData.getDictLabel()), DictData::getDictLabel, dictData.getDictLabel());
        queryWrapper.eq(StrUtil.isNotBlank(dictData.getDictStatus()), DictData::getDictStatus, dictData.getDictStatus());
        queryWrapper.orderByAsc(DictData::getDictValue);
        return super.page(pageModel, queryWrapper);
    }

    /**
     * 根据类型查询字典项
     *
     * @param parentId
     * @return
     */
    public List<DictData> getDictDataListByDictType(Long parentId) {
        LambdaQueryWrapper<DictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(DictData::getDictStatus, DictDataStatusEnum.NORMAL.code);
        queryWrapper.eq(DictData::getParentId,parentId);
        queryWrapper.orderByAsc(DictData::getDictSort);
        return list(queryWrapper);
    }
}
