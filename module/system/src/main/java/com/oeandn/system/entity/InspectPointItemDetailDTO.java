package com.oeandn.system.entity;

import com.oeandn.system.model.InspectPath;
import com.oeandn.system.model.InspectPoint;
import com.oeandn.system.model.InspectPointItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**

/**
 * 包名：com.oeandn.system.entity<br>
 * 文件名：InspectPointItemDetailDTO<br>
 * 版权：Copyright by xxxx公司<br>
 * 描述：<br>
 * 修改人：GGW<br>
 * 修改时间：2022-08-11 15:57<br>
 * 跟踪单号：<br>
 * 修改单号：<br>
 * 修改内容：<br>
 **/
@Data
@Accessors(chain = true)
@ApiModel(value="InspectPointItemDetailDTO", description="巡检项详情")
public class InspectPointItemDetailDTO {

    @ApiModelProperty(value = "巡检项详情")
    private InspectPointItem item;

    @ApiModelProperty(value = "修订记录")
    private List<InspectPointItem> items;

    @ApiModelProperty(value = "巡检点详情")
    private InspectPoint point;


}