package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.oeandn.common.model.PageModel;
import com.oeandn.system.model.InspectPath;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 巡检路线 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
public interface InspectPathMapper extends BaseMapper<InspectPath> {

    @Select(" SELECT " +
            " a.id, " +
            " a.path_type, " +
            " a.path_name, " +
            " a.path_code, " +
            " a.inspect_cycle, " +
            " a.inspect_on_duty, " +
            " a.sequence, " +
            " a.version, " +
            " a.summarize, " +
            " a.path_status, " +
            " a.create_by, " +
            " a.create_date, " +
            " a.update_by, " +
            " a.update_date, " +
            " a.remark, " +
            " a.dept_id, " +
            " b.business_id, " +
            " b.business_type, " +
            " b.approve_user_id, " +
            " b.approve_state  " +
            "FROM " +
            "  inspect_path AS a  " +
            "  LEFT JOIN approve_record AS b ON a.id = b.business_id  " +
            "  AND b.business_type = 1  " +
            "  ${ew.customSqlSegment}  ")
    IPage<InspectPath> pageListByEntity(IPage page, @Param("ew") Wrapper wrapper);
}
