package com.oeandn.system.mapper;

import com.oeandn.system.model.DangerZone;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 危险区-红橙区 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
public interface DangerZoneMapper extends BaseMapper<DangerZone> {

}
