package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.oeandn.system.model.Role;
import com.oeandn.system.model.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2020-08-19
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    /**
     * 根据用户id 查询已授权角色列表
     *
     * @param userId 用户id
     * @return
     */
    List<Role> selectAuthRoleListByUserId(@Param("userId") Long userId);

    /**
     * 分页查询
     *
     * @param page    分页对象
     * @param wrapper 条件参数
     * @return
     */
    IPage<UserInfo> selectPageList(IPage<UserInfo> page, @Param(Constants.WRAPPER) Wrapper wrapper);
}
