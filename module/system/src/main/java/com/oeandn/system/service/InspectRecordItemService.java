package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.oeandn.common.model.PageModel;
import com.oeandn.system.model.InspectRecord;
import com.oeandn.system.model.InspectRecordItem;
import com.oeandn.system.mapper.InspectRecordItemMapper;
import com.oeandn.db.base.service.BaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 巡检记录巡检项表 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Service
public class InspectRecordItemService extends BaseService<InspectRecordItemMapper, InspectRecordItem> {

    @Resource
    InspectRecordService inspectRecordService;
    /**
     * 获取表格数据
     *
     * @param inspectRecordItem
     * @param pageModel
     * @return
     */
    public IPage<InspectRecordItem> pageListByEntity(InspectRecordItem inspectRecordItem, PageModel pageModel) {
        LambdaQueryWrapper<InspectRecordItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(StrUtil.isNotBlank(inspectRecordItem.getInspectResult()), InspectRecordItem::getInspectResult, inspectRecordItem.getInspectResult());
        return super.page(pageModel, queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean saveOrUpdate(List<InspectRecordItem> inspectRecordItemList,String recordId,String dataComplete) {
        List<InspectRecordItem> entities = inspectRecordItemList;
        //循环插入巡检记录ID
        List<InspectRecordItem> inspectRecordItem = entities.stream().map(data -> {
            data.setRecordId(Long.valueOf(recordId));
            return data;
        }).collect(Collectors.toList());
        InspectRecord inspectRecord = new InspectRecord();
        inspectRecord.setId(Long.valueOf(recordId));
        inspectRecord.setDataComplete(dataComplete);
        inspectRecord.setStatus("3");
        if (inspectRecordItem.size()>0){
            //批量插入巡检记录巡检项表
            this.saveBatch(inspectRecordItem);
            //更新巡检记录表状态
            inspectRecordService.updateById(inspectRecord);
            return true;
        }
        return false;
    }
}
