package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.oeandn.common.model.TreeModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-16
 */
@Data
@Accessors(chain = true)
@TableName("sys_dept")
public class Dept implements TreeModel<Dept>, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 父部门id
     */
    private Long parentId;



    /**
     * 部门名称
     */
    private String name;

    /**
     * 显示顺序
     */
    private BigDecimal deptSort;



    /**
     * 部门状态（0、正常；1、停用-字典）
     */
    private String deptStatus;

    @TableField(exist = false)
    private List<Dept> children;


    public static final String ID = "id";

    public static final String PARENT_ID = "parent_id";

    public static final String ANCESTORS = "ancestors";

    public static final String NAME = "name";

    public static final String DEPT_SORT = "dept_sort";


    public static final String DEPT_STATUS = "dept_status";
}
