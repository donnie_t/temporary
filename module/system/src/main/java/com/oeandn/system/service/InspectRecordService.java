package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.common.model.PageModel;
import com.oeandn.system.entity.InspectRecordEntity;
import com.oeandn.system.model.InspectPath;
import com.oeandn.system.model.InspectRecord;
import com.oeandn.system.mapper.InspectRecordMapper;
import com.oeandn.db.base.service.BaseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检记录表 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Service
public class InspectRecordService extends BaseService<InspectRecordMapper, InspectRecord> {

    /**
     * 获取表格数据
     *
     * @param inspectRecord
     * @param pageModel
     * @return
     */
    public IPage<InspectRecord> pageListByEntity(InspectRecord inspectRecord, PageModel pageModel) {
        LambdaQueryWrapper<InspectRecord> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(inspectRecord.getDataComplete()), InspectRecord::getDataComplete, inspectRecord.getDataComplete());
        return super.page(pageModel, queryWrapper);
    }

    public Page<InspectRecordEntity> pageRecord(InspectRecordEntity inspectRecord, PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StrUtil.isNotBlank(inspectRecord.getPathName()),"pa."+InspectPath.PATH_NAME ,inspectRecord.getPathName());
        queryWrapper.like(StrUtil.isNotBlank(inspectRecord.getPathCode()),"pa."+InspectPath.PATH_CODE ,inspectRecord.getPathCode());
        queryWrapper.ge(StrUtil.isNotBlank(inspectRecord.getStartdate()),"re."+InspectRecord.START_DATE ,inspectRecord.getStartdate());
        queryWrapper.le(StrUtil.isNotBlank(inspectRecord.getEnddate()),"re."+InspectRecord.END_DATE ,inspectRecord.getEnddate());
        queryWrapper.like(StrUtil.isNotBlank(inspectRecord.getCreateby()),"re."+InspectRecord.CREATE_BY ,inspectRecord.getCreateby());
        queryWrapper.eq(StrUtil.isNotBlank(inspectRecord.getInspectOnDuty()),"pa."+InspectPath.INSPECT_ON_DUTY ,inspectRecord.getInspectOnDuty());
        queryWrapper.eq(StrUtil.isNotBlank(inspectRecord.getDatacomplete()),"re."+InspectRecord.DATA_COMPLETE ,inspectRecord.getDatacomplete());

        IPage page =new Page();
        page.setSize(pageModel.getSize());
        page.setCurrent(pageModel.getCurrent());
        return baseMapper.queryInspectPage(page,queryWrapper);
    }

}
