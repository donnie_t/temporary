package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.DictType;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-17
 */
public interface DictTypeMapper extends BaseMapper<DictType> {

}
