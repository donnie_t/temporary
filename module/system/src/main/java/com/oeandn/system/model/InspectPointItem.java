package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.oeandn.db.base.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * 巡检项
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class InspectPointItem extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 表主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 巡检点id
     */
    private Long inspectPointId;

    /**
     * 巡检位置
     */
    private String inspectPlace;

    /**
     * 设备码
     */
    private String deviceCode;

    /**
     * 巡检名称
     */
    private String itemName;

    /**
     * 属性code 
     */
    private String propertyCode;

    /**
     * 属性名称
     */
    private String propertyName;

    /**
     * 上限
     */
    private String upperLimit;

    /**
     * 下限
     */
    private String lowerLimit;

    /**
     * 高预警
     */
    private String highAlert;

    /**
     * 低预警
     */
    private String lowAlert;

    /**
     * 典型值
     */
    private String typicalValue;

    /**
     * 单位
     */
    private String unitName;

    /**
     * 异常偏离值
     */
    private String abnormalDeviationValue;

    /**
     * 是否历史记录0否1是
     */
    private String historyCode;

    /**
     * 历史记录id
     */
    private Long historyId;


    public static final String ID = "id";

    public static final String INSPECT_POINT_ID = "inspect_point_id";

    public static final String INSPECT_PLACE = "inspect_place";

    public static final String DEVICE_CODE = "device_code";

    public static final String ITEM_NAME = "item_name";

    public static final String PROPERTY_CODE = "property_code";

    public static final String PROPERTY_NAME = "property_name";

    public static final String UPPER_LIMIT = "upper_limit";

    public static final String LOWER_LIMIT = "lower_limit";

    public static final String HIGH_ALERT = "high_alert";

    public static final String LOW_ALERT = "low_alert";

    public static final String TYPICAL_VALUE = "typical_value";

    public static final String UNIT_NAME = "unit_name";

    public static final String ABNORMAL_DEVIATION_VALUE = "abnormal_deviation_value";

    public static final String HISTORY_CODE = "history_code";

    public static final String HISTORY_ID = "history_id";

}
