package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.RoleDept;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2021-03-29
 */
public interface RoleDeptMapper extends BaseMapper<RoleDept> {

}
