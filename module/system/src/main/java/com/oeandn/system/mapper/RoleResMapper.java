package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.RoleRes;

/**
 * <p>
 * 角色和资源关联表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2020-08-19
 */
public interface RoleResMapper extends BaseMapper<RoleRes> {

}
