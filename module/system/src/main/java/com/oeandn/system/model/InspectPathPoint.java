package com.oeandn.system.model;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * 路线巡检点关联表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class InspectPathPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 路线id
     */
    private Long pathId;

    /**
     * 巡检点id
     */
    private Long pointId;


    public static final String PATH_ID = "path_id";

    public static final String POINT_ID = "point_id";

}
