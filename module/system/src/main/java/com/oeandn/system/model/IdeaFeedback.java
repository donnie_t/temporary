package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.oeandn.db.base.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 意见反馈
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class IdeaFeedback extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 反馈类型
     */
    private String feedbackType;

    /**
     * 反馈内容
     */
    private String feedbackContent;

    @TableField(exist = false)
    private Date startDate;
    @TableField(exist = false)
    private Date endDate;


    public static final String ID = "id";

    public static final String FEEDBACK_TYPE = "feedback_type";

    public static final String FEEDBACK_CONTENT = "feedback_content";


}
