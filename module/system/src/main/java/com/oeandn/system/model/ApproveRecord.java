package com.oeandn.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.oeandn.db.base.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡检审批表
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ApproveRecord extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 业务主键id
     */
    private Long businessId;

    /**
     * 业务类型1巡检路线2巡检点3巡检记录4巡检项目
     */
    private String businessType;

    /**
     * 审批人id
     */
    private Long approveUserId;

    /**
     * 审批状态（0-待审批，1-审批通过，2-审批拒绝）
     */
    private String approveState;

    public static final String ID = "id";

    public static final String DEPT_ID = "dept_id";

    public static final String BUSINESS_ID = "business_id";

    public static final String BUSINESS_TYPE = "business_type";

    public static final String APPROVE_USER_ID = "approve_user_id";

    public static final String APPROVE_STATE = "approve_state";

}
