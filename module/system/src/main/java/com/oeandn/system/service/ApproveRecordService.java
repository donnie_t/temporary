package com.oeandn.system.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oeandn.common.model.PageModel;
import com.oeandn.common.model.PageResult;
import com.oeandn.db.base.service.BaseService;
import com.oeandn.system.entity.ApproveRevisionEntity;
import com.oeandn.system.entity.InspectResultRecordEntity;
import com.oeandn.system.entity.InspectPointApproveListDTO;
import com.oeandn.system.entity.PointApproveSelectDTO;
import com.oeandn.system.enums.ApproveRecordTypeEnum;
import com.oeandn.system.mapper.ApproveRecordMapper;
import com.oeandn.system.model.ApproveRecord;
import com.oeandn.system.model.InspectPoint;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 巡检审批表 服务实现类
 * </p>
 *
 * @author 饮水机管理员
 * @since 2022-08-09
 */
@Service
public class ApproveRecordService extends BaseService<ApproveRecordMapper, ApproveRecord> {


    /**
     * 巡检点
     */
    @Resource
    InspectPointService inspectPointService;
    /**
     * 巡检项
     */
    @Resource
    InspectPointItemService inspectPointItemService;
    /**
     * 巡检路线
     */
    @Resource
    InspectPathService inspectPathService;


    /**
     * 获取表格数据
     *
     * @param approveRecord
     * @param pageModel
     * @return
     */
    public IPage<ApproveRecord> pageListByEntity(ApproveRecord approveRecord, PageModel pageModel) {
        LambdaQueryWrapper<ApproveRecord> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(approveRecord.getBusinessType()), ApproveRecord::getBusinessType,
                approveRecord.getBusinessType());
        queryWrapper.like(StrUtil.isNotBlank(approveRecord.getApproveState()), ApproveRecord::getApproveState,
                approveRecord.getApproveState());
        return super.page(pageModel, queryWrapper);
    }


    /**
     * 功能描述:  添加1巡检路线2巡检点3巡检记录4巡检项目  发送审批
     *
     * @param deptId       部门id
     * @param id           业务主键id
     * @param businessId   业务类型1巡检路线2巡检点3巡检记录4巡检项目
     * @param approveState 审批状态（0-待审批，1-审批通过，2-审批拒绝）
     * @return {@link null}
     * @author GGW
     * @date 2022-08-12
     */
    public boolean saveApproveRecord(Long deptId, Long id, String businessId, String approveState) {

        ApproveRecord approveRecord = new ApproveRecord();

        approveRecord.setBusinessId(id);
        approveRecord.setBusinessType(businessId);
        approveRecord.setApproveState(approveState);
        approveRecord.setDeptId(deptId);
        approveRecord.setCreateDate(new Date());


        return save(approveRecord);
    }

    public Page<InspectResultRecordEntity> pageRecord(InspectResultRecordEntity inspectResultRecordEntity, PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StrUtil.isNotBlank(inspectResultRecordEntity.getPathName()),"c."+ InspectResultRecordEntity.PATH_NAME ,inspectResultRecordEntity.getPathName());
        queryWrapper.like(StrUtil.isNotBlank(inspectResultRecordEntity.getPathCode()),"c."+InspectResultRecordEntity.PATH_CODE ,inspectResultRecordEntity.getPathCode());
        queryWrapper.ge(StrUtil.isNotBlank(inspectResultRecordEntity.getStartdate()),"b."+ InspectResultRecordEntity.START_DATE ,inspectResultRecordEntity.getStartdate());
        queryWrapper.le(StrUtil.isNotBlank(inspectResultRecordEntity.getEnddate()),"b."+InspectResultRecordEntity.END_DATE ,inspectResultRecordEntity.getEnddate());
        queryWrapper.like(StrUtil.isNotBlank(inspectResultRecordEntity.getCreateBy()),"b."+InspectResultRecordEntity.CREATE_BY ,inspectResultRecordEntity.getCreateBy());
        queryWrapper.eq(StrUtil.isNotBlank(inspectResultRecordEntity.getApproveUserId()),"a."+InspectResultRecordEntity.APPROVEUSER_ID ,inspectResultRecordEntity.getApproveUserId());
        queryWrapper.eq(StrUtil.isNotBlank(inspectResultRecordEntity.getApproveState()),"a."+InspectResultRecordEntity.APPROVE_STATE ,inspectResultRecordEntity.getApproveState());
        queryWrapper.eq(StrUtil.isNotBlank(inspectResultRecordEntity.getPathType()),"c."+InspectResultRecordEntity.PATH_TYPE ,inspectResultRecordEntity.getPathType());

        IPage page =new Page();
        page.setSize(pageModel.getSize());
        page.setCurrent(pageModel.getCurrent());
        return baseMapper.queryApprovePage(page,queryWrapper);
    }

    public PageResult<InspectPointApproveListDTO> pointPage(PointApproveSelectDTO dto) {

        PageResult<InspectPointApproveListDTO> pageResult = new PageResult<>();

        Page<ApproveRecord> page = new Page<>();

        page.setSize(dto.getSize());
        page.setCurrent(dto.getCurrent());

        QueryWrapper<ApproveRecord> wrapper = new QueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(dto.getInspectPointName()),"ip."+ InspectPoint.INSPECT_POINT_NAME, dto.getInspectPointName());
        wrapper.eq(0 < dto.getApprovalStatus(),"a."+ApproveRecord.APPROVE_STATE, dto.getApprovalStatus());

        Page<InspectPointApproveListDTO> dtoPage = this.baseMapper.pointPage(page, wrapper);

        pageResult.setCurrent(dtoPage.getCurrent());
        pageResult.setPages(dtoPage.getPages());
        pageResult.setSize(dtoPage.getSize());
        pageResult.setTotal(dtoPage.getTotal());
        pageResult.setRecords(dtoPage.getRecords());
        return pageResult;
    }


    public boolean approve(ApproveRecord approveRecord) {


        ApproveRecord one = getOne(new QueryWrapper<ApproveRecord>().eq(ApproveRecord.BUSINESS_ID,
                        approveRecord.getBusinessId()).eq(ApproveRecord.BUSINESS_TYPE, approveRecord.getBusinessType()));
        if (StringUtils.isEmpty(one)) {
            if ("1".equals(approveRecord.getApproveState())) {

                //业务类型1巡检路线2巡检点3巡检记录4巡检项目
                if (ApproveRecordTypeEnum.POINT.code.equals(approveRecord.getBusinessType())) {
                    //巡检点
                    inspectPointService.upd(approveRecord.getBusinessId());
                }

                if (ApproveRecordTypeEnum.PROJECT.code.equals(approveRecord.getBusinessType())) {
                    //巡检项
                    inspectPointItemService.upd(approveRecord.getBusinessId());
                }

                if (ApproveRecordTypeEnum.PATH.code.equals(approveRecord.getBusinessType())) {
                    //巡检路线
                    inspectPathService.upd(approveRecord.getBusinessId());
                }
                //TODO 巡检结果
            }
            approveRecord.setId(one.getId());
            return updateById(approveRecord);

        }
        return false;
    }

    public Page<ApproveRevisionEntity> approveReisionPage(ApproveRevisionEntity approveRevisionEntity, PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(StrUtil.isNotBlank(approveRevisionEntity.getHistory_id()),"b."+ ApproveRevisionEntity.HISTORY_ID ,approveRevisionEntity.getHistory_id());
        IPage page =new Page();
        page.setSize(pageModel.getSize());
        page.setCurrent(pageModel.getCurrent());
        return baseMapper.approveReisionPage(page,queryWrapper);
    }
}
