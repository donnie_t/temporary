package com.oeandn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oeandn.system.model.Config;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author 饮水机管理员
 * @since 2019-09-06
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
