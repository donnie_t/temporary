package com.oeandn.db.base.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author：饮水机管理员
 * @Description: 公共实体类
 * @Date: 2019/9/6 13:55
 */
@Getter
@Setter
public class BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(hidden = true)
    private Long createBy;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(hidden = true)
    private Date createDate;

    /**
     * 部门id
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(hidden = true)
    private Long deptId;


    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(hidden = true)
    private Long updateBy;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(hidden = true)
    private Date updateDate;

    /**
     * 数据标识-0正常1删除2待审核（0：正常；1：已删除）
     *
     * @see com.oeandn.db.base.enums.BaseModelDelFlagEnum
     */
    @TableLogic
    @ApiModelProperty(hidden = true)
    private String delFlag;

    private String remark;

    @TableField(exist = false)
    private String createUserName;

    @TableField(exist = false)
    private String updateUserName;

    public static final String CREATE_BY = "create_by";

    public static final String CREATE_DATE = "create_date";

    public static final String UPDATE_BY = "update_by";

    public static final String UPDATE_DATE = "update_date";
    public static final String DEL_FLAG = "del_flag";

    public static final String REMARK = "remark";

}
