package com.oeandn.db.base.enums;

/**
 * @Author：饮水机管理员
 * @Description: 公共类数据标识-0正常1删除2待审核
 * @Date: 2021/9/2 15:28
 */
public enum BaseModelDelFlagEnum {

    //数据标识-0正常1删除2待审核（0：正常；1：已删除）
    NORMAL("0", "正常"),
    DELETED("1", "已删除"),
    ;

    public final String code;
    public final String name;

    BaseModelDelFlagEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

}
