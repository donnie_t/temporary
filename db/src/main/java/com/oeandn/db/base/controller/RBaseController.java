package com.oeandn.db.base.controller;

import com.oeandn.common.api.IErrorCode;
import com.oeandn.common.context.BaseContextHandler;
import com.oeandn.common.enums.ApiErrorCode;
import com.oeandn.common.model.R;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2021/4/20 16:30
 */
public class RBaseController {

    /**
     * 获取用户id
     */
    protected Long getUserId() {
        return BaseContextHandler.getUserID();
    }

    protected Long getDeptId() {
        return BaseContextHandler.getDeptID();
    }

    /**
     * 获取用户名称
     */
    protected String getUsername() {
        return BaseContextHandler.getUsername();
    }

    protected <T> R<T> success() {
        return R.restResult(ApiErrorCode.SUCCESS, null);
    }

    protected <T> R<T> success(T obj) {
        return R.success(obj);
    }

    protected <T> R<T> error(String msg) {
        return R.error(msg);
    }

    protected <T> R<T> error(IErrorCode errorCode, T obj) {
        return R.error(errorCode);
    }

    protected <T> R<T> restResult(IErrorCode errorCode, T data) {
        return R.restResult(errorCode, data);
    }

}
