package com.oeandn.db.base.service;

import com.oeandn.db.base.model.DataScopeRoleModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author：饮水机管理员
 * @Description:
 * @Date: 2021/9/3 14:24
 */
public interface IDataScopeService {

    /**
     * 是否有全部数据权限
     */
    String ALL_DATA_SCOPE_FLAG = "all-data-scope";

    /**
     * 角色列表
     */
    String ROLE_LIST = "role-list";

    /**
     * 获取角色列表和是否有全部数据权限
     *
     * @param userId 用户id
     * @return
     */
    Map<String, Object> getAllDataScopeFlagAndDataByUserId(Long userId);


    /**
     * 根据用户id和角色列表查询 获取所有的部门id
     *
     * @param userId   用户id
     * @param roleList 角色列表
     * @return
     */
    Set<Long> getDateScopeDeptIds(Long userId, List<DataScopeRoleModel> roleList);

    String getNickNameById(Long userId);
}
