package com.oeandn.db.config;

import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
public final class RedisCache implements Cache {

    /**
     * 读写锁
     */
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    /**
     * ID,获取缓存对象的唯一标识
     */
    private String id;

    /**
     * redisTemplate
     */
    private static RedisTemplate redisTemplate;

    public RedisCache(String id) {
        if (id == null) {
            throw new IllegalArgumentException("缓存实例需要一个id!");
        } else {
            log.info("开启Redis缓存: id = {}", id);
            this.id = id;
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public int getSize() {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            Long size = redisTemplate.opsForHash().size(this.id.toString());
            log.info("Redis缓存大小: id = {}, size = {}", id, size);
            return size.intValue();
        } catch (Exception e) {
            log.error("",e);
        }
        return 0;
    }

    @Override
    public void putObject(final Object key, final Object value) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            redisTemplate.opsForHash().put(this.id.toString(), key.toString(), value);
        } catch (Exception e) {
            log.error("",e);
        }
    }

    @Override
    public Object getObject(final Object key) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            Object hashVal = redisTemplate.opsForHash().get(this.id.toString(), key.toString());
            return hashVal;
        } catch (Exception e) {
            log.error("",e);
            return null;
        }
    }

    @Override
    public Object removeObject(final Object key) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            redisTemplate.opsForHash().delete(this.id.toString(), key.toString());
        } catch (Exception e) {
            log.error("",e);
        }
        return null;
    }

    @Override
    public void clear() {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            redisTemplate.delete(this.id.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    @Override
    public String toString() {
        return "RedisCache {" + this.id + "}";
    }

    /**
     * 由于启动期间注入失败，只能运行期间注入
     * donnie
     * @return
     */
    public RedisTemplate getRedisTemplate() {
        redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
        return redisTemplate;
    }

}

