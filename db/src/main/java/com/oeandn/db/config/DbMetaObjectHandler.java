package com.oeandn.db.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.oeandn.common.context.BaseContextHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author：饮水机管理员
 * @Description: 自动填充
 * @Date: 2019/9/6 10:44
 */
@Component
public class DbMetaObjectHandler implements MetaObjectHandler {

    private static final String CREATE_BY_FIELD = "createBy";
    private static final String CREATE_DATE_BY_FIELD = "createDate";
    private static final String UPDATE_BY_BY_FIELD = "updateBy";
    private static final String UPDATE_DATE_BY_FIELD = "updateDate";
    private static final String DEPT_BY_FIELD = "deptId";

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, CREATE_BY_FIELD, BaseContextHandler::getUserID, Long.class);
        this.strictInsertFill(metaObject, CREATE_DATE_BY_FIELD, Date::new, Date.class);
        this.strictInsertFill(metaObject, DEPT_BY_FIELD,BaseContextHandler::getDeptID, Long.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, UPDATE_BY_BY_FIELD, BaseContextHandler::getUserID, Long.class);
        this.strictUpdateFill(metaObject, UPDATE_DATE_BY_FIELD, Date::new, Date.class);
    }
}
